<?php

namespace SilexApp\Model\Services;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use SilexApp\lib\Database\Database;
use SilexApp\Model\Repository\PDOactionsRepository;
use DateTime;


use Symfony\Component\HttpFoundation\Response;
class SignupService
{
    private $repository;


    public function signupCheck(Application $app,Request $request){
        $estadoFinal = false;
        $flag = false;
        $f1 = false;
        $f2 = false;
        $f3 = false;
        $f4 = false;
        $f5 = false;

        $username = $request->get('username');
        $email = $request->get('email');
        $pass = $request->get('password');
        $confirm_pass = $request->get('cpassword');
        $bdate = $request->get('username');

        $birthdate1 = $request->get('date');

        $nowdate = date('Y-m-d');

        //$currdate = strtotime($nowdate);

        if($birthdate1>$nowdate){
            //fecha futura
            $flag   = true;
        }

        if(strlen($username>20)){
            $flag = true;
            $f1 = true;
        }

        if (ctype_alnum($username)) {
            //CORRECTE
        } else {
            $f1 = true;
            $flag = true;
        }

        //EMAIL
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            //Adreça valida
        } else {
            $flag = true;
            $f2 = true;
        }

        //PASS
        if(strlen($pass) < 6 || strlen($pass) > 12){
            $flag = true;
            $f3 = true;
        }
        if(preg_match('/[A-Z]/', $pass)){
            // There is one upper
        }else{
            $flag = true;
            $f3 = true;
        }
        if(preg_match('/[a-z]/',$pass)){
            // There is one lower
        }else {
            $flag = true;
            $f3 = true;
        }
        if (preg_match('/[^0-9]/', $pass))
        {
            // one or more of the 'special characters' found in $string
        }else{
            $flag = true;
            $f3 = true;
        }
        if($pass != $confirm_pass){
            $flag = true;
            $f4 = true;
        }

        $profilepic = $request->get('file_nm');

        if(is_null($profilepic)){

        }else{
            if(exif_imagetype($profilepic) != IMAGETYPE_JPEG || exif_imagetype($profilepic) != IMAGETYPE_PNG){
                $flag = true;
                $f5 = true;
            }
        }



        if($flag == false){
            //Tot COrrecte
            $dbs = Database::getInstance("PWGRAM", "root", "root");
            $this->repository = new PDOactionsRepository($dbs);
            $username = $request->get('username');
            $email = $request->get('email');
            $pass = $request->get('password');
            $pass = md5($pass);
            $birthdate1 = $request->get('date');
            $profilepic = $request->files->get('file_nm');


            //var_dump($profilepic1);
            if(!is_null($profilepic)){
                $profilepicName = $profilepic->getClientOriginalName();
                $picRealPath = $profilepic->getRealPath();
            }else{
                $profilepicName = null;
            }



            if(is_null($profilepicName)){
                $profilepicName = "default_profile.jpg";
                //var_dump($_FILES);
            }else{
                //Cogemos la imagen
                $imgForm =  $request->files->get("file_nm");
                $profilepic->move('assets/img/',$profilepicName);
                //var_dump($profilepicName);
                //move_uploaded_file($_FILES['file_nm']['tmp_name'], '/Applications/MAMP/htdocs/RepoPracticaFinal/web/assets/img/'.$_FILES['file_nm']['name']);
            }



            $active = 0;
            $image = null;
            $this->repository->signupUser($username, $email, $pass, $birthdate1, $profilepicName, $active, $image);
            $this->sendConfirmationEmail($username,$email);

            $id_user = $this->repository->getIDbyUserTable($username);

            setcookie("id",$id_user[0]["Id"], time()+3600*24);

        }else{
            $estadoFinal = true;

            if($f1){
                echo "<script type='text/javascript'>alert('INVALID FORMAT USERNAME');</script>";
            }
            if($f2){
                echo "<script type='text/javascript'>alert('INVALID EMAIL');</script>";
            }
            if($f3){
                echo "<script type='text/javascript'>alert('INVALID PASSWORD FORMAT');</script>";
            }
            if($f4){
                echo "<script type='text/javascript'>alert('PASSWORDS DO NOT MATCH');</script>";
            }
            if($f5){
                echo "<script type='text/javascript'>alert('IMAGE IS NOT JPG OR PNG');</script>";
            }


        }
        return $estadoFinal;
    }

    public function sendConfirmationEmail(string $username, string $email){
        $message = "
        Confirm your Email.
        Please click the link below to verify your account
        grup06.com/emailConfirmed/$username";

        mail($email,"Grup06 Confirm Email",$message,"From:donotReply@grup06.com" );
        //echo 'Mail Enviado!';
        //$content = $app['twig']->render('confirmationEmail.twig');
        //return $app->redirect('/confirmationEmail');

    }

}