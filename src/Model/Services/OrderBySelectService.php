<?php

namespace SilexApp\Model\Services;
use Silex\Application;
use SilexApp\lib\Database\Database;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SilexApp\Model\Repository\PDOactionsRepository;


use PDO;
use DateTime;
use SilexApp\Model\Services\SignupService;
use SilexApp\Model\Services\DataBaseService;


class OrderBySelectService{

    private $repository;

    public function orderByLikes(Application $app, $id){

        $dbs = Database::getInstance("PWGRAM","root","root");
        $this->repository = new PDOactionsRepository($dbs);

        $nom = $this->repository->getUsername($id);
        $numImg = $this->repository->getNumberImages($id);
        $numComment = $this->repository->getNumberComments($id);
        $listadoImgs = $this->repository->getImagesByLikes($id);


        $nomUser = $nom[0]["Username"];
        $numeroImgPublicadas = $numImg[0]["COUNT(*)"];
        $numeroComentarios = $numComment[0]["COUNT(*)"];


        $loggeado = false;
        if(empty($app['session']->get('user'))){

            $content = $app['twig']->render('publicProfileBlock8.twig', array('listadoImgs' => $listadoImgs,'user_id' => $id,'loggeado'=>$loggeado, 'nomUser' =>$nomUser, 'numImgs'=>$numeroImgPublicadas, 'numComentarios'=>$numeroComentarios));
            return $content;

        }else{
            $loggeado = true;
            $content = $app['twig']->render('publicProfileBlock8.twig', array('listadoImgs' => $listadoImgs,'user_id' => $id,'loggeado'=>$loggeado, 'nomUser' =>$nomUser, 'numImgs'=>$numeroImgPublicadas, 'numComentarios'=>$numeroComentarios));
            return $content;
        }


    }

    public function orderByVisits(Application $app, $id){

        $dbs = Database::getInstance("PWGRAM","root","root");
        $this->repository = new PDOactionsRepository($dbs);

        $nom = $this->repository->getUsername($id);
        $numImg = $this->repository->getNumberImages($id);
        $numComment = $this->repository->getNumberComments($id);
        $listadoImgs = $this->repository->getImagesByVisits($id);

        $user_id = $id;

        $nomUser = $nom[0]["Username"];
        $numeroImgPublicadas = $numImg[0]["COUNT(*)"];
        $numeroComentarios = $numComment[0]["COUNT(*)"];

        $loggeado = false;
        if(empty($app['session']->get('user'))){

            $content = $app['twig']->render('publicProfileBlock8.twig', array('listadoImgs' => $listadoImgs,'user_id' => $user_id, 'loggeado'=>$loggeado, 'nomUser' =>$nomUser, 'numImgs'=>$numeroImgPublicadas, 'numComentarios'=>$numeroComentarios));
            return $content;

        }else{
            $loggeado = true;
            $content = $app['twig']->render('publicProfileBlock8.twig', array('listadoImgs' => $listadoImgs,'user_id' => $user_id,'loggeado'=>$loggeado, 'nomUser' =>$nomUser, 'numImgs'=>$numeroImgPublicadas, 'numComentarios'=>$numeroComentarios));
            return $content;
        }


    }

    public function orderByComments(Application $app, $id){

        $dbs = Database::getInstance("PWGRAM","root","root");
        $this->repository = new PDOactionsRepository($dbs);

        $nom = $this->repository->getUsername($id);
        $numImg = $this->repository->getNumberImages($id);
        $numComment = $this->repository->getNumberComments($id);
        $listadoImgs = $this->repository->getNumberCommentsImg($id);

        $user_id = $id;
        $nomUser = $nom[0]["Username"];
        $numeroImgPublicadas = $numImg[0]["COUNT(*)"];
        $numeroComentarios = $numComment[0]["COUNT(*)"];

        $loggeado = false;
        if(empty($app['session']->get('user'))){

            $content = $app['twig']->render('publicProfileBlock8.twig', array('listadoImgs' => $listadoImgs,'user_id' => $user_id, 'loggeado'=>$loggeado, 'nomUser' =>$nomUser, 'numImgs'=>$numeroImgPublicadas, 'numComentarios'=>$numeroComentarios));
            return $content;

        }else{
            $loggeado = true;
            $content = $app['twig']->render('publicProfileBlock8.twig', array('listadoImgs' => $listadoImgs,'user_id' => $user_id,'loggeado'=>$loggeado, 'nomUser' =>$nomUser, 'numImgs'=>$numeroImgPublicadas, 'numComentarios'=>$numeroComentarios));
            return $content;
        }
    }

    public function orderByData(Application $app, $id){
        $dbs = Database::getInstance("PWGRAM","root","root");
        $this->repository = new PDOactionsRepository($dbs);

        $nom = $this->repository->getUsername($id);
        $numImg = $this->repository->getNumberImages($id);
        $numComment = $this->repository->getNumberComments($id);
        $listadoImgs = $this->repository->getImagesFromUserByCreation($id);

        $user_id = $app['session']->get('user');

        $nomUser = $nom[0]["Username"];
        $numeroImgPublicadas = $numImg[0]["COUNT(*)"];
        $numeroComentarios = $numComment[0]["COUNT(*)"];

        $loggeado = false;
        if(empty($app['session']->get('user'))){

            $content = $app['twig']->render('publicProfileBlock8.twig', array('listadoImgs' => $listadoImgs,'user_id' => $user_id, 'loggeado'=>$loggeado, 'nomUser' =>$nomUser, 'numImgs'=>$numeroImgPublicadas, 'numComentarios'=>$numeroComentarios));
            return $content;

        }else{
            $loggeado = true;
            $content = $app['twig']->render('publicProfileBlock8.twig', array('listadoImgs' => $listadoImgs,'user_id' => $user_id,'loggeado'=>$loggeado, 'nomUser' =>$nomUser, 'numImgs'=>$numeroImgPublicadas, 'numComentarios'=>$numeroComentarios));
            return $content;
        }
    }

}