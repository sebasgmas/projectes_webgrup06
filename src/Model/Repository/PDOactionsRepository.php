<?php

namespace SilexApp\Model\Repository;

use SilexApp\lib\Database\Database;

/**
 * PdoTasksRepository
 */
class PDOactionsRepository implements actionsRepository
{
    /**
     * The database class
     * @var Database
     */
    private $db;

    /**
     * PdoTasksRepository constructor.
     * @param Database $db
     */
    public function __construct(Database $db)
    {
        $this->db = $db;
    }


    /**
     * Persist the given task into the database
     * @param Task $task
     */
    function signupUser($username, $email, $pass, $birthdate, $profilepic, $active, $image)
    {

        $query = "INSERT INTO User(Username,Email,Birthdate,Password,Img_path,Active) VALUES (?,?,?,?,?,?)";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $username,
                $email,
                $birthdate,
                $pass,
                $profilepic,
                $active
            ]
        );
    }

    function getNotifications(int $id){

        $query = "SELECT * FROM Notification WHERE UserIdTo = ? AND Seen = 0";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );

        return $statement->fetchAll();
    }

    function getAll(){
        $statement = $this->db->query(
            "SELECT * FROM Image ORDER BY id DESC"
        );
        return $statement->fetchAll();
    }

    function retrieveProfile($Id_user){
        $query = "SELECT * FROM User WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $Id_user
            ]
        );
        return $statement->fetchAll();
    }

    function retrieveLogInUser($nick, $pass){
        $query = "SELECT * FROM User WHERE Username = ? AND Password = ? AND Active = 1";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $nick,
                $pass
            ]
        );

        return $statement->fetchAll();
    }

    function retrieveLogInEmail($nick, $pass){
        $query = "SELECT * FROM User WHERE Email = ? AND Password = ? AND Active = 1";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $nick,
                $pass
            ]
        );

        return $statement->fetchAll();
    }

    function updateUserData($id_user, $username, $birthdate, $pass, $profilepic){
        $query = "UPDATE User SET Username = ?, Birthdate = ?, Password = ?, Img_path = ? WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $username,
                $birthdate,
                $pass,
                $profilepic,
                $id_user
            ]
        );
    }

    function subirImg($title, $private, $user_id, $profilepic, $visits, $created_at, $userString){

        $query = "INSERT INTO `Image`(`User_id`, `Title`, `Img_path`, `Visits`, `Private`, `Created_at`, `Username`) VALUES (?,?,?,?,?,?,?)";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $user_id,
                $title,
                $profilepic,
                $visits,
                $private,
                $created_at,
                $userString
            ]
        );
    }

    function getImgUser($id_user){
        $query = "SELECT * FROM Image WHERE User_id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id_user
            ]
        );
        return $statement->fetchAll();
    }

    function getImgPathFromUserId($id){
        $query = "SELECT Img_path FROM User WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
        return $statement->fetchAll();
    }

    function removeImage($id){
        $query = "DELETE FROM Image where Id=?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
    }

    function getImageToModify($id){
        $query = "SELECT * FROM Image WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
        return $statement->fetchAll();
    }

    function modifyDataPic($title, $private, $id, $profilepic){
        $query = "UPDATE Image SET Title = ?, Img_path = ? ,Private= ? WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $title,
                $profilepic,
                $private,
                $id
            ]
        );
    }

    function getPopping(){
        $statement = $this->db->query(
            "SELECT * FROM Image WHERE Private = 0 ORDER BY Visits DESC LIMIT 5"
        );
        return $statement->fetchAll();

    }


    function getUsername($id){
        $query = "SELECT Username FROM User WHERE Id=?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
        return $statement->fetchAll();
    }

    function getNumberImages($id){
        $query = "SELECT COUNT(*) from Image WHERE User_id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
        return $statement->fetchAll();
    }

    function getNumberComments($id){
        $query = "SELECT COUNT(*) from Comment WHERE Id_user = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
        return $statement->fetchAll();
    }

    function getImagesFromUserByCreation($id){
        $query = "SELECT * from Image WHERE User_id = ? AND Private = 0 ORDER BY Created_at DESC";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
        return $statement->fetchAll();
    }

    function getImagesByLikes($id){
        $query = "SELECT * from Image WHERE User_id = ? AND Private = 0 ORDER BY Likes DESC";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
        return $statement->fetchAll();
    }

    function getImagesByVisits($id){
        $query = "SELECT * from Image WHERE User_id = ? AND Private = 0 ORDER BY Visits DESC";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
        return $statement->fetchAll();
    }

    function getNumberCommentsImg($id){
        $query = "SELECT * from Image WHERE User_id = ? ORDER BY NumComments DESC";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
        return $statement->fetchAll();
    }

    function getCommentsFromUser($id){
        $query = "SELECT Id_img from Comment WHERE Id_user = ? GROUP BY Id_img DESC";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
        return $statement->fetchAll();
    }

    function checkUserComment($id, $id_user, $contenido){
        $query = "SELECT * FROM Comment WHERE Id_user = ? AND Id_img = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id_user,
                $id
            ]
        );
        return $statement->fetchAll();
    }

    function addComment($id, $id_user, $contenido, $created_at,$userIdTo){
        $query = "INSERT INTO Comment (Id_img, Id_user, Content, Created_at, Id_userTo) VALUES (?,?,?,?,?)";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id,
                $id_user,
                $contenido,
                $created_at,
                $userIdTo
            ]
        );
    }

    function getNumCommentsValueFromImg($id_img){

        $query ="UPDATE Image SET NumComments = NumComments + 1 WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id_img
            ]
        );
    }

    function addVisit($id){
        $query = "UPDATE Image SET Visits = Visits + 1 WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
    }

    function getIDbyUserTable($id){
        $query = "SELECT Id FROM User WHERE Username=?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
        return $statement->fetchAll();
    }

    function getUserId($id){
        $query = "SELECT User_id FROM Image WHERE Id=?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
        return $statement->fetchAll();
    }

    function getPost($id){
        $query = "SELECT * FROM Image WHERE Id=?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
        return $statement->fetchAll();
    }

    function getComments($userID,$id){
        $query = "SELECT * FROM Comment WHERE Id_img=? LIMIT 3";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
        return $statement->fetchAll();
    }

    function getAllCommentsFromUser($id_user){
        $query = "SELECT * FROM Comment WHERE Id_user=?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id_user
            ]
        );
        return $statement->fetchAll();
    }

    function deleteComment($id){
        $query ="DELETE FROM Comment WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
    }

    function editComment($id){
        $query = "SELECT * FROM Comment WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
        return $statement->fetchAll();
    }

    function saveEditComment($id, $contenido){
        $query = "UPDATE Comment SET Content = ? WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $contenido,
                $id
            ]
        );
    }

    function sendNotificationToUser($id, $id_user, $tipo,$userIdTo){
        $query = "INSERT INTO Notification (Id_img, Id_user, NotifType,UserIdTo) VALUES (?,?,?,?)";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id,
                $id_user,
                $tipo,
                $userIdTo
            ]
        );
    }

    function getNotifUsername($id_user){
        $query = "SELECT Username FROM User WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id_user
            ]
        );
        return $statement->fetchAll();
    }

    function getNotifTitleImg($id_img){
        $query = "SELECT Title FROM Image WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id_img
            ]
        );
        return $statement->fetchAll();
    }

    function checkSeenNotification($id){
        $query = "UPDATE Notification SET Seen = 1 WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
    }

    function getIdimgFromIdComment($id){
        $query = "SELECT Id_img FROM Comment WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
        return $statement->fetchAll();
    }

    function decreaseNumCommentsInImage($id_img){
        $query = "UPDATE Image SET NumComments = NumComments - 1 WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id_img
            ]
        );
    }

    function getLastAddedImages(){
        $query = "SELECT * FROM Image WHERE Private = 0 ORDER BY Created_at DESC LIMIT 5";
        $statement = $this->db->preparedQuery(
            $query,
            [
            ]
        );
        return $statement->fetchAll();
    }

    function getLastCommentFromImg($id_img){
        $query = "SELECT Content, Id_img FROM Comment WHERE Id_img = ? ORDER BY Created_at DESC LIMIT 1";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id_img
            ]
        );
        return $statement->fetchAll();
    }

    function getPathFromLoggedUser($id_CurrentUser){
        $query = "SELECT Img_path FROM User WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id_CurrentUser
            ]
        );
        return $statement->fetchAll();
    }

    function confirmedEmail($id){
        $query = "UPDATE User Set Active=1 where Username=?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
    }

    function getAllFromUser($id){
        $query = "SELECT * FROM Image WHERE User_id= ? ORDER BY id DESC";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
        return $statement->fetchAll();
    }

    function darLike($img_id, $user_id){
        $query = "INSERT INTO Likes (Id_img, Id_User) VALUES (?,?)";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $img_id,
                $user_id
            ]
        );
    }

    function getLikes($img_id, $user_id){
        $query = "SELECT * FROM Likes WHERE Id_img= ? AND id_User = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $img_id,
                $user_id
            ]
        );
        return $statement->fetchAll();
    }

    function getImageLikes($img_id){
        $query = "SELECT Likes FROM Image WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $img_id
            ]
        );
        return $statement->fetchAll();
    }

    function addLike($id){
        $query = "UPDATE Image SET Likes = Likes + 1 WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
    }

    function restarLike($id){
        $query = "UPDATE Image SET Likes = Likes - 1 WHERE Id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );
    }

    function getImgAjax($index){
        $query = "SELECT * FROM Image WHERE Private = 0  ORDER BY Visits LIMIT $index,5";

        $statement = $this->db->preparedQuery(
            $query,
            [
                $index
            ]
        );

        return $statement->fetchAll();
    }

    function getImgAjaxNewPics($index){
        $query = "SELECT * FROM Image WHERE Private = 0  ORDER BY Created_at DESC LIMIT $index,5";

        $statement = $this->db->preparedQuery(
            $query,
            [
                $index
            ]
        );

        return $statement->fetchAll();
    }

    function getCommentsAjax($index, $id_img){
        $query = "SELECT * FROM Comment WHERE Id_img = ? ORDER BY Created_at LIMIT $index,3";

        $statement = $this->db->preparedQuery(
            $query,
            [
                $id_img
            ]
        );

        return $statement->fetchAll();
    }

    function eliminarUserFromTable($id_user){
        $query = "DELETE FROM Likes WHERE id_User = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id_user
            ]
        );
    }

    function updateUserDataTableImage($id_user, $username){
        $query = "UPDATE Image SET Username = ? WHERE User_id = ?";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $username,
                $id_user
            ]
        );
    }

    function senNotificationLike($id_img, $id_user, $tipo, $userIdTo){
        $query = "INSERT INTO Notification (Id_img, Id_user, NotifType, UserIdTo) VALUES (?,?,?,?)";
        $statement = $this->db->preparedQuery(
            $query,
            [
                $id_img,
                $id_user,
                $tipo,
                $userIdTo
            ]
        );

    }

    function getUserIdFromImageId($id){
        $query = "SELECT User_id FROM Image WHERE Id = ?";

        $statement = $this->db->preparedQuery(
            $query,
            [
                $id
            ]
        );

        return $statement->fetchAll();
    }

    /**
     * Return all the tasks of the database
     * @return array

    public function getAll(): array
    {
        $statement = $this->db->query(
            "SELECT * from tasks ORDER BY id DESC"
        );
        return $statement->fetchAll();
    }
     * */
}
