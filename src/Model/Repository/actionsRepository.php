<?php
namespace SilexApp\Model\Repository;



interface actionsRepository
{
    public function signupUser($username, $email, $pass, $birthdate, $profilepic, $active, $image);
    public function getNotifications(int $id);
    public function getAll();
    public function retrieveLogInUser($nick, $pass);
    public function retrieveLogInEmail($nick, $pass);
    public function retrieveProfile($Id_user);
    public function updateUserData($id_user, $username, $birthdate, $pass, $profilepic);
    public function subirImg($title, $private, $user_id, $profilepic, $visits, $created_at, $username);
    public function getImgUser($id_user);
    public function getImageToModify($id);
    public function modifyDataPic($title, $private, $id,$profilepic);
    public function getPopping();
    public function getUsername($id);
    public function getNumberImages($id);
    public function getNumberComments($id);
    public function getImagesByLikes($id);
    public function getImagesByVisits($id);
    public function getCommentsFromUser($id);
    public function getNumberCommentsImg($id);
    public function checkUserComment($id, $id_user, $contenido);
    public function addComment($id, $id_user, $contenido, $created_at,$userIdTo);
    public function addVisit($id);
    public function getUserId($id);
    public function getPost($id);
    public function getComments($userID,$id);
    public function getAllCommentsFromUser($id_user);
    public function deleteComment($id);
    public function editComment($id);
    public function saveEditComment($id, $contenido);
    public function sendNotificationToUser($id, $id_user, $tipo,$userIdTo);
    public function getNotifUsername($id_user);
    public function getNotifTitleImg($id_img);
    public function checkSeenNotification($id);
    public function getIdimgFromIdComment($id);
    public function decreaseNumCommentsInImage($id_img);
    public function getLastAddedImages();
    public function getLastCommentFromImg($id_img);
    public function confirmedEmail($id);
    public function getAllFromUser($id);
    public function getIDbyUserTable($id);
    public function darLike($img_id, $user_id);
    public function getImageLikes($img_id);
    public function restarLike($id);
    public function addLike($id);
    public function getImgPathFromUserId($id);
    public function getImgAjax($index);
    public function getCommentsAjax($index,$id_img);
    public function getImgAjaxNewPics($index);
    public function eliminarUserFromTable($id_user);
    public function updateUserDataTableImage($id_user, $username);
    public function senNotificationLike($id_img, $id_user, $tipo, $userIdTo);
    public function getUserIdFromImageId($id);
}
