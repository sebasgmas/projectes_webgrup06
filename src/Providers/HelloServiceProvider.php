<?php
/**
 * Created by PhpStorm.
 * User: sebastianmiguelgelabertmas
 * Date: 05/04/2017
 * Time: 19:14
 */
namespace SilexApp\Providers;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/*ESTA CLASE LO QUE HACE ES LO QUE HARIA EL CONTROLADOR, Y EN EL CONTROLADOR LLAMAREMOS A $APP[HELLO]*/

class HelloServiceProvider implements ServiceProviderInterface
{

    public function register(Container $app){
        $app['hello'] = $app->protect(function ($name) use ($app){
            $default = $app['hello.default_name'] ? $app['hello.default_name'] : '';
            //si name existe, name = name sino default
            $name = $name ?: $default;

            /*return $app['twig']->render('hello.twig',array(
                'user' => $name,
                'app' => [
                    'name' => $app['app.name']
                ]
            ));*/
        });
    }
}