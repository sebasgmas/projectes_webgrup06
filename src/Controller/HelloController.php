<?php

namespace SilexApp\Controller;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SilexApp\lib\Database\Database;
use DateTime;
use SilexApp\Model\Services\SignupService;
use SilexApp\Model\Services\DataBaseService;
use SilexApp\Model\Repository\PDOactionsRepository;

class HelloController{

    private $request;
    private $repository;
    private $viewHelper;

    public function indexAction(Application $app, Request $request){
       //envio
        $dbs = Database::getInstance("PWGRAM", "root", "root");
        $this->repository = new PDOactionsRepository($dbs);
        $arrayGeneral = array();
        $currentUser_path = "default_profile.jpg";
        //modificamos el getPopping para que ahora solo muestre imagenes mas vistas POR AHORA LIMITAMOS LA QUERY A LIMIT 5
        $posts = $this->repository->getPopping();
        //PEDIMOS A LA BASE QUE NOS DE LAS 5 ULTIMAS IMAGENES AÑADIDAS EN LA APLICACION
        $lastPosts = $this->repository->getLastAddedImages();
        //creamos funcion de la BBDD que devuelva la profile pic del user loggeado
        for($i = 0; $i < count($lastPosts); $i++){
            $id_img = $lastPosts[$i]["Id"];
            $lastComments = $this->repository->getLastCommentFromImg($id_img);
            if(!empty($lastComments)){
                $lastCommentString = $lastComments[0]["Content"];
                $Id_imgRelacionada = $lastComments[0]["Id_img"];
                //creamos el array de objeto username y titulo
                $a = array('Content', 'Id_img');
                $b = array($lastCommentString, $Id_imgRelacionada);
                $c = array_combine($a, $b);
                array_push($arrayGeneral,$c);
            }
        }

        $loggeado = false;
        $notificaciones = false;
        //miramos si tenemos la session iniciada
        if(empty($app['session']->get('user'))){
            //$content = $app['twig']->render('home.twig', array('s' => array($app['app.hLog'],$app['app.titleLog'])));
            $content = $app['twig']->render('home.twig', array('loggeado' => $loggeado,'profilePic' => $currentUser_path,'lastArray' => $lastPosts, 'arrayGeneral' => $arrayGeneral,'posts' => $posts, 'notificaciones' => $notificaciones));

            $response = new Response();
            $response->setStatusCode($response::HTTP_OK);
            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);
            return $response;
        }else{
           $loggeado = true;
            $img_path = $this->repository->getPathFromLoggedUser($app['session']->get('user'));
            $currentUser_path = $img_path[0]["Img_path"];
           //*****************NOTIFICACIONES*********
            $id_user = $app['session']->get('user');
            $arrayNotificaciones = $this->repository->getNotifications($id_user);

            if(count($arrayNotificaciones) > 0){
                $notificaciones = true;
                //$content = $app['twig']->render('home.twig', array('loggeado' => $loggeado,'hLog' => ['hLog' => $app['app.hLog']],'titleLog' => ['titleLog' => $app['app.titleLog']],'hRegister' => ['hRegister' => $app['app.hRegister']],'titleRegister' => ['titleRegister' => $app['app.titleRegister']],'hOwner' => ['hOwner' => $app['app.vacio']],'titleOwner' => ['titleOwner' => $app['app.vacio']],'posts' => $posts, 'notificaciones' => $notificaciones));
            }
            //***************************************
            $userID = $app['session']->get('user');

            //var_dump($id);
            $userName = $this->repository->getUsername($userID);
            $content = $app['twig']->render('home.twig', array('loggeado' => $loggeado,'userId' =>$userID, 'profilePic' => $currentUser_path,'lastArray' => $lastPosts, 'arrayGeneral' => $arrayGeneral, 'userSession' => $userName[0]["Username"],'posts' => $posts, 'notificaciones' => $notificaciones));

            $response = new Response();
            $response->setStatusCode($response::HTTP_OK);
            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);
            return $response;

            //$content = $app['twig']->render('home.twig', array('hNew' => array($app['app.hLog'],$app['app.titleLog'])));
        }
       return null;
    }

    public function showConfirmation(Application $app,Request $request){
        //envio
        $loggeado = false;
        if(!empty($app['session']->get('user'))) {
            $loggeado = true;
        }

        $content = $app['twig']->render('confirmationEmail.twig', array('loggeado' => $loggeado));


        $response = new Response();
        $response->setStatusCode($response::HTTP_OK);
        $response->headers->set('Content-Type', 'text/html');
        $response->setContent($content);

        return $response;
    }

    public function confirmedEmail(Application $app,$id){
        $dbs = Database::getInstance("PWGRAM", "root", "root");
        $this->repository = new PDOactionsRepository($dbs);


        $loggeado = false;
        if(!empty($app['session']->get('user'))) {
            $loggeado = true;
        }
        $userID = $this->repository->getIDbyUserTable($id);

        $app['session']->set('user', $userID[0]["Id"]);

        $img_path = $this->repository->getPathFromLoggedUser($app['session']->get('user'));
        $currentUser_path = $img_path[0]["Img_path"];
        $arrayNotificaciones = $this->repository->getNotifications($app['session']->get('user'));

        $notificaciones = false;
        if(count($arrayNotificaciones) > 0){
            $notificaciones = true;
        }
        $tasks = $this->repository->confirmedEmail($id);
        $content = $app['twig']->render('emailConfirmed.twig',array('loggeado' => $loggeado,'profilePic' => $currentUser_path,'userSession' => $id,'notificaciones' => $notificaciones));

        $response = new Response();
        $response->setStatusCode($response::HTTP_OK);
        $response->headers->set('Content-Type', 'text/html');
        $response->setContent($content);

        return $app->redirect('/');
    }



    public function userPage(Application $app,Request $request){
        $dbs = Database::getInstance("PWGRAM", "root", "root");
        $this->repository = new PDOactionsRepository($dbs);


        $userID = $app['session']->get('user');

        $posts = $dbs->getAllFromUser($userID);

        $userName = $dbs->getUsername($userID);
        $content = $app['twig']->render('userPage.twig', array('userSession' => $userName,'hLog' => ['hLog' => $app['app.vacio']],'titleLog' => ['titleLog' => $app['app.vacio']],'hRegister' => ['hRegister' => $app['app.vacio']],'titleRegister' => ['titleRegister' => $app['app.vacio']],'hOwner' => ['hOwner' => $app['app.hOwner']],'titleOwner' => ['titleOwner' => $app['app.titleOwner']],'posts' => $posts));
        $response = new Response();
        $response->setStatusCode($response::HTTP_OK);
        $response->headers->set('Content-Type', 'text/html');
        $response->setContent($content);

        return $response;
    }



    public function likePost($id,Application $app, Request $request){
        $dbs = Database::getInstance("PWGRAM", "root", "root");
        $this->repository = new PDOactionsRepository($dbs);

        $posts = $this->repository->getPopping();

        if(empty($app['session']->get('user'))){
            $loggeado = false;
            //$content = $app['twig']->render('home.twig', array('s' => array($app['app.hLog'],$app['app.titleLog'])));
            $content = $app['twig']->render('home.twig', array('loggeado' => $loggeado,'hLog' => ['hLog' => $app['app.hLog']],'titleLog' => ['titleLog' => $app['app.titleLog']],'hRegister' => ['hRegister' => $app['app.hRegister']],'titleRegister' => ['titleRegister' => $app['app.titleRegister']],'hOwner' => ['hOwner' => $app['app.vacio']],'titleOwner' => ['titleOwner' => $app['app.vacio']],'posts' => $posts));

            $response = new Response();
            $response->setStatusCode($response::HTTP_OK);
            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);
            return $response;
        }else{
            $loggeado = true;
            $userID = $app['session']->get('user');
            $dbs = new DataBaseService();
            //var_dump($id);
            $userName = $dbs->getUsername($userID);
            $dbs->likePost($id);
            $posts = $dbs->getPoppin();
            $content = $app['twig']->render('home.twig', array('loggeado' => $loggeado,'userSession' => $userName,'hLog' => ['hLog' => $app['app.vacio']],'titleLog' => ['titleLog' => $app['app.vacio']],'hRegister' => ['hRegister' => $app['app.vacio']],'titleRegister' => ['titleRegister' => $app['app.vacio']],'hOwner' => ['hOwner' => $app['app.hOwner']],'titleOwner' => ['titleOwner' => $app['app.titleOwner']],'posts' => $posts));

            $response = new Response();
            $response->setStatusCode($response::HTTP_OK);
            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);
            return $response;
        }
    }
}