<?php

namespace SilexApp\Controller;


use Silex\Application;
use SilexApp\lib\Database\Database;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SilexApp\Model\Repository\PDOactionsRepository;

class NotificationsController
{

    private $repository;

    public function loadAndShowNotifications(Application $app){
        $dbs = Database::getInstance("PWGRAM", "root", "root");
        $this->repository = new PDOactionsRepository($dbs);

        //controlamos que este loggeado
        if(empty($app['session']->get('user'))){
            //VARGAR TEMPLATE DE CODIGO DE ERROR$message = 'Access Forbidden. ERROR ';
            $message = 'Access Forbidden. ERROR ';
            $response = new Response();
            $response->setStatusCode($response::HTTP_FORBIDDEN);
            $code = $response->getStatusCode();

            $content = $app['twig']->render('error.twig', array('code' => $code,'message' => $message));


            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);
            return $response;
        }else {


            $arrayGeneral = array();
            $id_user = $app['session']->get('user');
            $arrayNotifications = $this->repository->getNotifications($id_user);



            for ($i = 0; $i < count($arrayNotifications); $i++) {
                $aux_user = $arrayNotifications[$i]["Id_user"];
                $username = $this->repository->getNotifUsername($aux_user);
                $userNameFinal = $username[0]["Username"];

                $aux_img = $arrayNotifications[$i]["Id_img"];
                $titleImg = $this->repository->getNotifTitleImg($aux_img);
                $titleFinal = $titleImg[0]["Title"];

                //creamos el array de objeto username y titulo
                $a = array('Username', 'Title', 'Id_notif', 'Tipo', );
                $b = array($userNameFinal, $titleFinal, $arrayNotifications[$i]["Id"],$arrayNotifications[$i]["NotifType"]);
                $c = array_combine($a, $b);
                array_push($arrayGeneral, $c);
            }



            $vacio = 1;
            if (empty($arrayGeneral)) {
                $vacio = 0;
            }

            $loggeado = false;
            if (!empty($app['session']->get('user'))) {
                $loggeado = true;
            }
            $selectImage = $this->repository->getImgPathFromUserId($app['session']->get('user'));
            $userName = $this->repository->getUsername($app['session']->get('user'));
            $arrayNotificaciones = $this->repository->getNotifications($app['session']->get('user'));

            $notificaciones = false;
            if(count($arrayNotificaciones) > 0){
                $notificaciones = true;
                //$content = $app['twig']->render('home.twig', array('loggeado' => $loggeado,'hLog' => ['hLog' => $app['app.hLog']],'titleLog' => ['titleLog' => $app['app.titleLog']],'hRegister' => ['hRegister' => $app['app.hRegister']],'titleRegister' => ['titleRegister' => $app['app.titleRegister']],'hOwner' => ['hOwner' => $app['app.vacio']],'titleOwner' => ['titleOwner' => $app['app.vacio']],'posts' => $posts, 'notificaciones' => $notificaciones));
            }
            $profilePic = $selectImage[0]["Img_path"];

            $content = $app['twig']->render('notification.twig', array('array' => $arrayGeneral,'notificaciones' => $notificaciones, 'vacio' => $vacio, 'loggeado' => $loggeado, 'profilePic' => $profilePic, 'userSession' => $userName[0]["Username"]));
            $response = new Response();
            $response->setContent($content);

            return $response;

        }
    }

    public function checkSeenNotification(Application $app, $id){
        $dbs = Database::getInstance("PWGRAM", "root", "root");
        $this->repository = new PDOactionsRepository($dbs);

        $this->repository->checkSeenNotification($id);

        return $app->redirect('/showNotifications');
    }

}