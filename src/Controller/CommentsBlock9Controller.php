<?php

namespace SilexApp\Controller;

use Silex\Application;
use SilexApp\lib\Database\Database;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SilexApp\Model\Repository\PDOactionsRepository;
use DateTime;

class CommentsBlock9Controller
{
    private $repository;

    public function validateComment($id, Application $app, Request $request)
    {

        $dbs = Database::getInstance("PWGRAM", "root", "root");
        $this->repository = new PDOactionsRepository($dbs);

        $id_user = $app['session']->get('user');
        $logeado = true;
        $contenido = $request->get('comentario');

        $contenidoEscapado = strip_tags($contenido);
        //antes de añadir un comentario a la base de datos revisamos que el usurio no haya comentado ya en la misma imagen
        $notCommentedBefore = $this->repository->checkUserComment($id, $id_user, $contenidoEscapado);
        if (empty($notCommentedBefore)){
            $now = new DateTime();
            $created_at = $now->format('Y-m-d H:i:s');
            $userIdTo = $this->repository->getUserIdFromImageId($id);
            $this->repository->addComment($id, $id_user, $contenidoEscapado, $created_at,$userIdTo[0]["User_id"]);
            //VAMOS A INCREMENTAR EL VALOR DE NUMCOMMENT DE LA TABLA IMAGEN PARA DICHA IMAGEN COMENTADA
            $this->repository->getNumCommentsValueFromImg($id);
            $tipo = 0;
            //enviamos notificacion al usuario de la imagen
            $this->repository->sendNotificationToUser($id, $id_user, $tipo, $userIdTo[0]["User_id"]);
        }
        $response = new Response();
        $response->setStatusCode($response::HTTP_OK);
        $response->headers->set('Content-Type', 'text/html');

        return $app->redirect('/');
        //return $response;
    }

    public function commentsList(Application $app, Request $request)
    {
        $dbs = Database::getInstance("PWGRAM", "root", "root");
        $this->repository = new PDOactionsRepository($dbs);

        //controlamos que este loggeado
        if(empty($app['session']->get('user'))){
            //VARGAR TEMPLATE DE CODIGO DE ERROR$message = 'Access Forbidden. ERROR ';
            $message = 'Access Forbidden. ERROR ';
            $response = new Response();
            $response->setStatusCode($response::HTTP_FORBIDDEN);
            $code = $response->getStatusCode();

            $content = $app['twig']->render('error.twig', array('code' => $code,'message' => $message));


            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);
            return $response;
        }else {

            $id_user = $app['session']->get('user');
            $arrayComments = $this->repository->getAllCommentsFromUser($id_user);

            $vacio = 1;
            if (empty($arrayGeneral)) {
                $vacio = 0;
            }

            $loggeado = false;
            if (!empty($app['session']->get('user'))) {
                $loggeado = true;
            }
            $selectImage = $this->repository->getImgPathFromUserId($app['session']->get('user'));
            $userName = $this->repository->getUsername($app['session']->get('user'));
            $arrayNotificaciones = $this->repository->getNotifications($app['session']->get('user'));
            $notificaciones = false;
            if(count($arrayNotificaciones) > 0){
                $notificaciones = true;
            }
            $profilePic = $selectImage[0]["Img_path"];

            $content = $app['twig']->render('comment.twig', array('comments' => $arrayComments,'notificaciones' => $notificaciones, 'loggeado' => $loggeado, 'profilePic' => $profilePic, 'userSession' => $userName[0]["Username"]));
            $response = new Response();
            $response->setStatusCode($response::HTTP_OK);
            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);

            return $response;

        }


    }

    public function publicPost(Application $app,Request $request,$id){
        //%id --> es id de la img
        $dbs = Database::getInstance("PWGRAM", "root", "root");
        $this->repository = new PDOactionsRepository($dbs);

        $visits = $this->repository->addVisit($id);
        //var_dump($visits);
        $userID =  $this->repository->getUserId($id);
        $userName = $this->repository->getUsername($userID[0]["User_id"]);

        $post =  $this->repository->getPost($id);
        $comments = $this->repository->getComments($userID,$id);
        $your_date = $post[0]["Created_at"];
        //var_dump($your_date);
        $date1 = new DateTime($your_date);
        //var_dump($date1);
        $date2 = new DateTime("now");
        //var_dump($date2);

        $interval = $date2->diff($date1);
        //var_dump($interval);

        $days = $interval->days;
        //var_dump($days);

        //creamos el array de objeto username y titulo
        $arrayGeneral = array();
        for ($i = 0; $i <count($comments); $i++) {
            $username = $this->repository->getUsername($comments[$i]["Id_user"]);
            $a = array('Username','Content');
            $b = array($username[0]["Username"], $comments[$i]["Content"]);
            $c = array_combine($a, $b);
            array_push($arrayGeneral,$c);
        }


        $loggeado = false;
        if(empty($app['session']->get('user'))){

            if($post[0]['Private'] == 1){
                //Intenta acceder a una img privada
                //VARGAR TEMPLATE DE CODIGO DE ERROR
                $message = 'Access Forbidden. ERROR ';
                $response = new Response();
                $response->setStatusCode($response::HTTP_FORBIDDEN);
                $code = $response->getStatusCode();

                $content = $app['twig']->render('error.twig', array('code' => $code,'message' => $message));


                $response->headers->set('Content-Type', 'text/html');
                $response->setContent($content);
                return $response;


            }else{
                $content = $app['twig']->render('publicPost.twig', array('datediff' =>$days,'loggeado'=>$loggeado,'username' => $userName[0]["Username"],'userId' => $userID, 'post' => $post,'comments' =>$arrayGeneral));
                $response = new Response();
                $response->setStatusCode($response::HTTP_OK);
                $response->headers->set('Content-Type', 'text/html');
                $response->setContent($content);

                return $response;
            }


        }else{

            $img_path = $this->repository->getPathFromLoggedUser($app['session']->get('user'));
            $currentUser_path = $img_path[0]["Img_path"];
            $userID = $app['session']->get('user');
            $userName = $this->repository->getUsername($userID);
            $arrayNotificaciones = $this->repository->getNotifications($userID);
            $notificaciones = false;
            if(count($arrayNotificaciones) > 0){
                $notificaciones = true;
                //$content = $app['twig']->render('home.twig', array('loggeado' => $loggeado,'hLog' => ['hLog' => $app['app.hLog']],'titleLog' => ['titleLog' => $app['app.titleLog']],'hRegister' => ['hRegister' => $app['app.hRegister']],'titleRegister' => ['titleRegister' => $app['app.titleRegister']],'hOwner' => ['hOwner' => $app['app.vacio']],'titleOwner' => ['titleOwner' => $app['app.vacio']],'posts' => $posts, 'notificaciones' => $notificaciones));
            }

            $loggeado = true;
            $posts = $this->repository->retrieveProfile($app['session']->get('user'));
            $loggeado = true;

            //$content = $app['twig']->render('publicPost.twig', array('datediff' =>$datediff,'loggeado'=>$loggeado,'username' => $userName[0]['Username'],'userId' => $userID[0]["User_id"],'post' => $post,'comments' =>$arrayGeneral));
            $content = $app['twig']->render('publicPost.twig', array('datediff' =>$days,'loggeado'=>$loggeado,'username' => $userName[0]['Username'],'userId' => $userID,'post' => $post,'comments' =>$arrayGeneral,'array' => $posts, 'notificaciones' => $notificaciones,'userSession' => $userName[0]["Username"],'profilePic' => $currentUser_path,'loggeado' => $loggeado));

            $response = new Response();
            $response->setStatusCode($response::HTTP_OK);
            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);

            return $response;
        }
    }

    public function deleteComment(Application $app, $id){
        $dbs = Database::getInstance("PWGRAM", "root", "root");
        $this->repository = new PDOactionsRepository($dbs);

        //antes de eliminar un comentario, hay que coger el id_img asociado al Id del comentario para poder decrementar el valor NumComments en la tabla IMAGEN
        $id_img = $this->repository->getIdimgFromIdComment($id);
        //decrementamos el NumComments
        $this->repository->decreaseNumCommentsInImage($id_img[0]["Id_img"]);
        $this->repository->deleteComment($id);

        return $app->redirect('/getComment');
    }

    public function editComment(Application $app, $id){
        $dbs = Database::getInstance("PWGRAM", "root", "root");
        $this->repository = new PDOactionsRepository($dbs);
        //controlamos que este loggeado
        if(empty($app['session']->get('user'))){
            //VARGAR TEMPLATE DE CODIGO DE ERROR$message = 'Access Forbidden. ERROR ';
            $message = 'Access Forbidden. ERROR ';
            $response = new Response();
            $response->setStatusCode($response::HTTP_FORBIDDEN);
            $code = $response->getStatusCode();

            $content = $app['twig']->render('error.twig', array('code' => $code,'message' => $message));


            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);
            return $response;
        }else {
            $loggeado = false;
            if (!empty($app['session']->get('user'))) {
                $loggeado = true;
            }
            $selectImage = $this->repository->getImgPathFromUserId($app['session']->get('user'));
            $userName = $this->repository->getUsername($app['session']->get('user'));
            $arrayNotificaciones = $this->repository->getNotifications($app['session']->get('user'));
            $notificaciones = false;
            if(count($arrayNotificaciones) > 0){
                $notificaciones = true;
                //$content = $app['twig']->render('home.twig', array('loggeado' => $loggeado,'hLog' => ['hLog' => $app['app.hLog']],'titleLog' => ['titleLog' => $app['app.titleLog']],'hRegister' => ['hRegister' => $app['app.hRegister']],'titleRegister' => ['titleRegister' => $app['app.titleRegister']],'hOwner' => ['hOwner' => $app['app.vacio']],'titleOwner' => ['titleOwner' => $app['app.vacio']],'posts' => $posts, 'notificaciones' => $notificaciones));
            }
            $profilePic = $selectImage[0]["Img_path"];

            $comment = $this->repository->editComment($id);

            $content = $app['twig']->render('editComment.twig', array('comentario' => $comment,'notificaciones' => $notificaciones,'loggeado' => $loggeado, 'profilePic' => $profilePic, 'userSession' => $userName[0]["Username"]));
            $response = new Response();
            $response->setStatusCode($response::HTTP_OK);
            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);

            return $response;

        }
    }

    public function saveEditComment(Application $app,Request $request, $id){
        $dbs = Database::getInstance("PWGRAM", "root", "root");
        $this->repository = new PDOactionsRepository($dbs);

        $contenido = $request->get('textName');
        $contenidoEscapado = strip_tags($contenido);

        $this->repository->saveEditComment($id, $contenidoEscapado);

        return $app->redirect('/');
    }

}