<?php


namespace SilexApp\Controller;


use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use SilexApp\lib\Database\Database;
use DateTime;
use SilexApp\Model\Services\SignupService;
use SilexApp\Model\Services\DataBaseService;
use SilexApp\Model\Repository\PDOactionsRepository;

class AjaxController
{

    private $repository;

    function get_persons(Application $app, Request $request)
    {

        $dbs = Database::getInstance("PWGRAM", "root", "root");
        $this->repository = new PDOactionsRepository($dbs);

        $index = $request->get('limit');

        //hacemos query
        $arrayImagenes = $this->repository->getImgAjax($index);


        $mivariable = json_encode($arrayImagenes);
        echo $mivariable;

        return new Response();

    }


    function get_newPics(Application $app, Request $request){
        //getImgAjaxNewPics
        $dbs = Database::getInstance("PWGRAM", "root", "root");
        $this->repository = new PDOactionsRepository($dbs);

        $index = $request->get('limit');

        //hacemos query
        $arrayImagenes = $this->repository->getImgAjaxNewPics($index);

        $arrayGeneral = array();
        for($i = 0; $i < count($arrayImagenes); $i++){
            $id_img = $arrayImagenes[$i]["Id"];
            $lastComments = $this->repository->getLastCommentFromImg($id_img);
            if(empty($lastComments)){
                $lastCommentString = " ";
            }else{
                $lastCommentString = $lastComments[0]["Content"];
            }
            $a = array('Id','User_id','Title','Img_path','Visits','Private','Created_at','Likes','NumComments','Username','LastComment');
            $b = array($arrayImagenes[$i]['Id'],$arrayImagenes[$i]['User_id'],$arrayImagenes[$i]['Title'],$arrayImagenes[$i]['Img_path'],$arrayImagenes[$i]['Visits'],$arrayImagenes[$i]['Private'],$arrayImagenes[$i]['Created_at'],$arrayImagenes[$i]['Likes'],$arrayImagenes[$i]['NumComments'],$arrayImagenes[$i]['Username'],$lastCommentString);
            $c = array_combine($a, $b);
            array_push($arrayGeneral,$c);
        }

        //var_dump($arrayGeneral);


        $mivariable = json_encode($arrayGeneral);
        echo $mivariable;

        return new Response();

    }



    function get_comments(Application $app, Request $request)
    {

        $dbs = Database::getInstance("PWGRAM", "root", "root");
        $this->repository = new PDOactionsRepository($dbs);

        $index = $request->get('limit');
        $id_img = $request->get('id_img');


        $arrayComments = $this->repository->getCommentsAjax($index,$id_img);


        $arrayGeneral = array();
        for ($i = 0; $i <count($arrayComments); $i++) {
            $username = $this->repository->getUsername($arrayComments[$i]["Id_user"]);
            $a = array('Username','Content');
            $b = array($username[0]["Username"], $arrayComments[$i]["Content"]);
            $c = array_combine($a, $b);
            array_push($arrayGeneral,$c);
        }



        //hacemos query
        //$arrayImagenes = $this->repository->getCommentsAjax($index, );



        $mivariable = json_encode($arrayGeneral);
        echo $mivariable;

        return new Response();

    }


}