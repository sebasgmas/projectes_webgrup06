<?php

namespace SilexApp\Controller;
use Silex\Application;
use SilexApp\lib\Database\Database;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SilexApp\Model\Repository\PDOactionsRepository;
//en esta clase se manejaran todas las funciones que esten relacionadas con el registro y/o login
class  AccessLogin{


    private $repository;


    public function renderLogIn(Application $app){
        $loggeado = false;
        if(!empty($app['session']->get('user'))) {
            $loggeado = true;
        }

        $content = $app['twig']->render('logIn.twig', array('loggeado' => $loggeado,'hLog' => ['hLog' => $app['app.vacio']],'titleLog' => ['titleLog' => $app['app.vacio']],'hRegister' => ['hRegister' => $app['app.vacio']],'titleRegister' => ['titleRegister' => $app['app.vacio']],'hOwner' => ['hOwner' => $app['app.hOwner']],'titleOwner' => ['titleOwner' => $app['app.titleOwner']]));
        $response = new Response();
        $response->setStatusCode($response::HTTP_OK);
        $response->headers->set('Content-Type', 'text/html');
        $response->setContent($content);

        return $response;
    }

    public function validateAction(Application $app, Request $request){
        $aux = 0;

        $dbs = Database::getInstance("PWGRAM", "root", "root");
        $this->repository = new PDOactionsRepository($dbs);

        $nick = $request->get('username');
        $pass = $request->get('password');

        $pass1 = md5($pass);
        $popUp = 1;

        //VOLVEMOS A FILTRAR LA INFORMACION QUE NOS LLEGA DEL "CLIENTE" Y DIFERENCIAMOS ENTRE USERNAME O EMAIL
        if(!empty($nick)){
            if (filter_var($nick, FILTER_VALIDATE_EMAIL)) {
                //echo 'Es un email';
                $aux = 1;
            }else if (preg_match("/^[a-zA-Z1-9]+$/", $nick)) {
                //echo 'Es un username';
                $aux = 2;
            }else{
                echo "<script type='text/javascript'>alert('Format of credentials not valid');</script>";
                $response = new Response();
                $response->setStatusCode($response::HTTP_FORBIDDEN);
                return $app->redirect('/logIn');
            }
        }

        if($aux == 1){
            $results = $this->repository->retrieveLogInEmail($nick, $pass1);

        }else if($aux == 2){
            $results = $this->repository->retrieveLogInUser($nick, $pass1);

        }

        $aux = 0;

        if(!empty($results)){
            //echo 'exito al loggearse';
            $app['session']->set('user', $results[0]["Id"]);
            return $app->redirect('/');
        }else{
            //echo 'fracaso al loggearse';
            // header("Status: 403 Forbidden");
            //$app->abort(403, 'No ha sido posible iniciar sesion.');
            echo "<script type='text/javascript'>alert('Wrong credentials or account not activated');</script>";
            return $app->redirect('/logIn');
        }
        $content = new Response(Response::HTTP_FORBIDDEN);
        $response = new Response();
        $response->setStatusCode($response::HTTP_OK);
        return $response;
    }
}