<?php
/**
 * Created by PhpStorm.
 * User: sebastianmiguelgelabertmas
 * Date: 08/05/2017
 * Time: 19:49
 */

namespace SilexApp\Controller;
use Silex\Application;
use SilexApp\Model\Repository\PDOactionsRepository;
use SilexApp\Model\Services\SignupService;

use SilexApp\lib\Database\Database;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProfileData
{

    private $repository;

    public function getDataToModify(Application $app){
        $dbs = Database::getInstance("PWGRAM","root","root");
        $this->repository = new PDOactionsRepository($dbs);
        $posts = $this->repository->retrieveProfile($app['session']->get('user'));


        if(empty($app['session']->get('user'))){
            //VARGAR TEMPLATE DE CODIGO DE ERROR
            //$code = 'ERROR 403';
            $message = 'Access Forbidden. ERROR ';
            $response = new Response();
            $response->setStatusCode($response::HTTP_FORBIDDEN);
            $code = $response->getStatusCode();

            $content = $app['twig']->render('error.twig', array('code' => $code,'message' => $message));


            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);
            return $response;

        }else{
            $img_path = $this->repository->getPathFromLoggedUser($app['session']->get('user'));
            $currentUser_path = $img_path[0]["Img_path"];
            $userID = $app['session']->get('user');
            $userName = $this->repository->getUsername($userID);
            $arrayNotificaciones = $this->repository->getNotifications($userID);
            $notificaciones = false;
            if(count($arrayNotificaciones) > 0){
                $notificaciones = true;
                //$content = $app['twig']->render('home.twig', array('loggeado' => $loggeado,'hLog' => ['hLog' => $app['app.hLog']],'titleLog' => ['titleLog' => $app['app.titleLog']],'hRegister' => ['hRegister' => $app['app.hRegister']],'titleRegister' => ['titleRegister' => $app['app.titleRegister']],'hOwner' => ['hOwner' => $app['app.vacio']],'titleOwner' => ['titleOwner' => $app['app.vacio']],'posts' => $posts, 'notificaciones' => $notificaciones));
            }

            $loggeado = true;
            $content = $app['twig']->render('userProfile.twig', array( 'array' => $posts, 'notificaciones' => $notificaciones,'userSession' => $userName[0]["Username"],'profilePic' => $currentUser_path,'loggeado' => $loggeado));

        }



        //$content = $app['twig']->render('userProfile.twig', array('array' => $posts));
        $response = new Response();
        $response->setContent($content);
        return $response;

    }

    public function botoncito(Application $app, Request $request){
        $dbs = Database::getInstance("PWGRAM","root","root");
        $this->repository = new PDOactionsRepository($dbs);
        $username = $request->get('username');
        $birthdate = $request->get('bday');
        $pass = $request->get('password');
        $confirm_pass = $request->get('cpassword');
        $backup = $request->get('backUpPassword');
        $backupImgPath = $request->get('backUpImgPath');
        $flagPass = false;
        $flagImg = false;
        //var_dump($pass);
        if(empty($pass)){
            //No han escrito nada y quieren mantener la password
            $flagPass = true;
        }

        //$profilepic = $request->get('file_nm');
        $id_user = $app['session']->get('user');



        $profilepic1 = $request->files->get('file_nm');
        //var_dump($profilepic1);
        //var_dump($profilepic1);

        if(is_null($profilepic1)){
            $profilepic = $request->get('backUpImgPath');
            //var_dump($profilepic);
        }else{
            if(exif_imagetype($profilepic1) != IMAGETYPE_JPEG){
                $flag = true;
                $profilepic = $request->get('backUpImgPath');
            }else{
                $profilepic = $profilepic1->getClientOriginalName();
                $picRealPath = $profilepic1->getRealPath();
                var_dump($picRealPath);
                $profilepic1->move('assets/img/',$profilepic);
            }

        }

        $flag = false;
        if(strlen($username>20)){
            $flag = true;
        }

        if (ctype_alnum($username)) {
            //CORRECTE
        } else {
            $flag = true;
            echo "INVALID USERNAME";
        }

        if($flagPass == false){
            //PASS
            if(strlen($pass) < 6 || strlen($pass) > 12){
                $flag = true;
                echo "INVALID PASSWORD \n";
            }
            if(preg_match('/[A-Z]/', $pass)){
                // There is one upper
            }else{
                echo "THE PASSWORD DOES NOT CONTAIN AN UPPERCASE LETTER \n";
                $flag = true;
            }
            if(preg_match('/[a-z]/',$pass)){
                // There is one lower
            }else {
                echo "THE PASSWORD DOES NOT CONTAIN A LOWERCASE LETTER \n";
                $flag = true;
            }
            if (preg_match('/[^0-9]/', $pass))
            {
                // one or more of the 'special characters' found in $string
            }else{
                echo "THE PASSWORD DOES NOT CONTAIN A NUMERIC CHARACTER \n";
                $flag = true;
            }
            if($pass != $confirm_pass){
                echo "THE TWO PASSWORDS DO NOT MATCH \n";
                $flag = true;
            }
        }



        if($flag == false){
            if($flagPass){

                $this->repository->updateUserData($id_user, $username, $birthdate, $backup, $profilepic);
                $this->repository->updateUserDataTableImage($id_user, $username);
                echo "<script type='text/javascript'>alert('CHANGES APPLIED');</script>";

            }else{
                $newPass = md5($pass);
                $this->repository->updateUserData($id_user, $username, $birthdate, $newPass, $profilepic);
                $this->repository->updateUserDataTableImage($id_user, $username);
                echo "<script type='text/javascript'>alert('CHANGES APPLIED');</script>";

            }
        }else{
            echo "<script type='text/javascript'>alert('ERROR. TRY AGAIN!');</script>";
            return $app->redirect('/editProfileData');
        }


        return $app->redirect('/editProfileData');

    }

}