<?php
/**
 * Created by PhpStorm.
 * User: sebastianmiguelgelabertmas
 * Date: 08/05/2017
 * Time: 19:03
 */

namespace SilexApp\Controller;
use Silex\Application;
use SilexApp\Model\Services\SignupService;

use SilexApp\lib\Database\Database;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SilexApp\Model\Repository\PDOactionsRepository;


class AccessSignUp{

    private $repository;

    public function showSignup(Application $app,Request $request){
        $username = null;
        $primera_vez = false;
        $loggeado = false;
        $email = false;
        if(!empty($app['session']->get('user'))) {
            $loggeado = true;
        }
        //envio
        $content = $app['twig']->render('signup.twig', array('primera_vez' =>$primera_vez,'username' =>$username,'loggeado' => $loggeado,'email'=>$email));

        $response = new Response();
        $response->setStatusCode($response::HTTP_OK);
        $response->headers->set('Content-Type', 'text/html');
        $response->setContent($content);

        return $response;
    }

    public function checkSignup(Application $app,Request $request){
        //envio
        $primera_vez = true;
        $loggeado = false;
        if(!empty($app['session']->get('user'))) {
            $loggeado = true;
        }
        //$username = $request->get('username');
        //$email = $request->get('email');
        //$content = $app['twig']->render('signup.twig',array('primera_vez' =>$primera_vez,'loggeado' => $loggeado));

        $signupService = new SignupService();
        $estadoFinal = $signupService->signupCheck($app,$request);

        if($estadoFinal == false){
            //$response = new Response();
            //$response->setStatusCode($response::HTTP_OK);
            //$response->headers->set('Content-Type', 'text/html');
            //$response->setContent($content);

            return $app->redirect('/confirmationEmail');
        }else{

            //Cogemos las variables para llenar el form
            $username = $request->get('username');
            $email = $request->get('email');


            $content = $app['twig']->render('signup.twig',array('primera_vez' =>$primera_vez,'loggeado' => $loggeado, 'username' =>$username, 'email'=>$email));
            $response = new Response();
            $response->setStatusCode($response::HTTP_FORBIDDEN);
            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);
            return $response;
            //return $app->redirect('/signup');
        }


    }

}