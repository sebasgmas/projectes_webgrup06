<?php
/**
 * Created by PhpStorm.
 * User: sebastianmiguelgelabertmas
 * Date: 08/05/2017
 * Time: 23:38
 */

namespace SilexApp\Controller;
use Silex\Application;
use SilexApp\lib\Database\Database;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SilexApp\Model\Repository\PDOactionsRepository;


class PublicPostsController{

   private $repository;

    public function publicPostsPoppin(Application $app, Request $request){

        $array = array();
        $dbs = Database::getInstance("PWGRAM","root","root");
        $this->repository = new PDOactionsRepository($dbs);


        $posts = $this->repository->getPopping();
        $arr = array_values($posts);


        if(empty($app['session']->get('user'))){
            $loggeado = false;
            $notificaciones = false;
            $userName[0]["Username"] = null;
            $currentUser_path = null;
            $content = $app['twig']->render('publicPostsPoppin.twig', array('notificaciones' => $notificaciones,'userSession' => $userName[0]["Username"],'profilePic' => $currentUser_path,'posts' => $posts, 'loggeado' =>$loggeado));
            $response = new Response();
            $response->setStatusCode($response::HTTP_OK);
            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);
            return $response;

        }else{
            $img_path = $this->repository->getPathFromLoggedUser($app['session']->get('user'));

            //var_dump($app['session']->get('user'));
            $currentUser_path = $img_path[0]["Img_path"];

            $loggeado = true;


            $userID = $app['session']->get('user');
            $userName = $this->repository->getUsername($userID);
            $arrayNotificaciones = $this->repository->getNotifications($userID);
            $notificaciones = false;
            if(count($arrayNotificaciones) > 0){
                $notificaciones = true;
                //$content = $app['twig']->render('home.twig', array('loggeado' => $loggeado,'hLog' => ['hLog' => $app['app.hLog']],'titleLog' => ['titleLog' => $app['app.titleLog']],'hRegister' => ['hRegister' => $app['app.hRegister']],'titleRegister' => ['titleRegister' => $app['app.titleRegister']],'hOwner' => ['hOwner' => $app['app.vacio']],'titleOwner' => ['titleOwner' => $app['app.vacio']],'posts' => $posts, 'notificaciones' => $notificaciones));
            }

            $userID = $app['session']->get('user');
            $username = $this->repository->getUsername($userID);
            $content = $app['twig']->render('publicPostsPoppin.twig', array('posts' => $posts, 'loggeado' =>$loggeado, 'username' => $username,'publicPostsPoppin.twig','notificaciones' => $notificaciones,'userSession' => $userName[0]["Username"],'profilePic' => $currentUser_path));
            $response = new Response();
            $response->setStatusCode($response::HTTP_OK);
            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);
            return $response;
        }
    }
}