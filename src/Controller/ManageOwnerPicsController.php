<?php

namespace SilexApp\Controller;
use Silex\Application;
use SilexApp\lib\Database\Database;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SilexApp\Model\Repository\PDOactionsRepository;

class ManageOwnerPicsController{


    public function getMyPics(Application $app){
        $dbs = Database::getInstance("PWGRAM","root","root");
        $this->repository = new PDOactionsRepository($dbs);

        $id = $app['session']->get('user');
        $posts = $this->repository->getImgUser($id);


        if(empty($app['session']->get('user'))){
            //VARGAR TEMPLATE DE CODIGO DE ERROR$message = 'Access Forbidden. ERROR ';
            $message = 'Access Forbidden. ERROR ';
            $response = new Response();
            $response->setStatusCode($response::HTTP_FORBIDDEN);
            $code = $response->getStatusCode();

            $content = $app['twig']->render('error.twig', array('code' => $code,'message' => $message));


            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);
            return $response;
        }else{
            $img_path = $this->repository->getPathFromLoggedUser($app['session']->get('user'));
            $currentUser_path = $img_path[0]["Img_path"];
            $userID = $app['session']->get('user');
            $userName = $this->repository->getUsername($userID);
            $arrayNotificaciones = $this->repository->getNotifications($userID);
            $notificaciones = false;
            if(count($arrayNotificaciones) > 0){
                $notificaciones = true;
                //$content = $app['twig']->render('home.twig', array('loggeado' => $loggeado,'hLog' => ['hLog' => $app['app.hLog']],'titleLog' => ['titleLog' => $app['app.titleLog']],'hRegister' => ['hRegister' => $app['app.hRegister']],'titleRegister' => ['titleRegister' => $app['app.titleRegister']],'hOwner' => ['hOwner' => $app['app.vacio']],'titleOwner' => ['titleOwner' => $app['app.vacio']],'posts' => $posts, 'notificaciones' => $notificaciones));
            }

            $loggeado = true;
            $content = $app['twig']->render('editImage.twig', array( 'posts' => $posts, 'notificaciones' => $notificaciones,'userSession' => $userName[0]["Username"],'profilePic' => $currentUser_path,'loggeado' => $loggeado));

        }


        $response = new Response();
        $response->setContent($content);

        return $response;


    }

    public function removeImage(Application $app,Request $request,$id){
        $dbs = Database::getInstance("PWGRAM","root","root");
        $this->repository = new PDOactionsRepository($dbs);
        $this->repository->removeImage($id);

        return $app->redirect('/getMyPics');
    }

    public function editImage_owner(Application $app,Request $request, $id){
        $dbs = Database::getInstance("PWGRAM","root","root");
        $this->repository = new PDOactionsRepository($dbs);
        $image = $this->repository->getImageToModify($id);
        $content = $app['twig']->render('owner_editImage.twig', ['image' => $image]);
        $response = new Response();
        $response->setStatusCode($response::HTTP_OK);
        $response->headers->set('Content-Type', 'text/html');
        $response->setContent($content);

        return $response;

    }

    public function validateAndUpdatePic(Application $app,Request $request, $id){
        $dbs = Database::getInstance("PWGRAM","root","root");
        $this->repository = new PDOactionsRepository($dbs);
        $backupImgPath = $request->get('backUpImgPath');


        $flag = false;
        $title = $request->get('title');

        $flagTitle = false;
        //miramos si ha modificado el titulo
        if(empty($title)){
            $flagTitle = true;
            $title = $request->get('backUpTitle');
        }else{
            //validamos el titulo
            if(strlen($title>20)){
                $flag = true;
            }

            if (ctype_alnum($title)) {
                //CORRECTE
            } else {
                $flag = true;
            }
        }

        $private = $request->get('private');
        if($private == null){
            $private = 0;

        }else{
            $private = 1;
        }

        $profilepic1 = $request->files->get('file_nm');
        $profilepic = "";

        if(is_null($profilepic1)){

            $profilepic = $request->get('backUpImgPath');
            //var_dump($profilepic);
        }else{
            if(exif_imagetype($profilepic1) != IMAGETYPE_JPEG){
                $flag = true;
                $profilepic = $request->get('backUpImgPath');
            }else{
                $profilepic = $profilepic1->getClientOriginalName();
                $picRealPath = $profilepic1->getRealPath();
                //var_dump($picRealPath);
                $profilepic1->move('assets/img/',$profilepic);
            }

        }


        if($flag == false){
            //var_dump($profilepic);
            $this->repository->modifyDataPic($title, $private, $id,$profilepic);
        }else{
            echo "<script type='text/javascript'>alert('Error al modificar la imagen, prueba de nuevo');</script>";
            return $app->redirect("/edit/$id");
        }

        return $app->redirect("/getMyPics");
    }

}