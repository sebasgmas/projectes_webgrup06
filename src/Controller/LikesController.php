<?php
/**
 * Created by PhpStorm.
 * User: sebastianmiguelgelabertmas
 * Date: 16/05/2017
 * Time: 14:58
 */

namespace SilexApp\Controller;



use Silex\Application;
use SilexApp\lib\Database\Database;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SilexApp\Model\Repository\PDOactionsRepository;

class LikesController
{

    private $repository;

    function likePressed(Application $app, $id_img, $id_user, $id_pagina){
        // $id_pagina == 0 --> se ha dado like desde la home
        //sino desde la visualizacion publica


        $dbs = Database::getInstance("PWGRAM", "root", "root");
        $this->repository = new PDOactionsRepository($dbs);

        $loggeado = false;
        if(empty($app['session']->get('user'))) {

        }else {



            $loggeado = true;
            $likeDado = $this->repository->getLikes($id_img, $id_user);
            if (empty($likeDado)) {

                //No le ha dado like
                $this->repository->darLike($id_img, $id_user);
                $this->repository->addLike($id_img);
                $userIdTo = $this->repository->getUserIdFromImageId($id_img);

                //enviamos notificacion
                $tipo = 1;
                $this->repository->senNotificationLike($id_img, $id_user, $tipo,$userIdTo[0]["User_id"]);




            } else {
                //Ya le ha dado Like
                $this->repository->restarLike($id_img);
                $this->repository->eliminarUserFromTable($id_user);
            }

        }



        return $app->redirect('/');

    }

}