<?php
/**
 * Created by PhpStorm.
 * User: sebastianmiguelgelabertmas
 * Date: 09/05/2017
 * Time: 10:34
 */

namespace SilexApp\Controller;
use Silex\Application;
use SilexApp\lib\Database\Database;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SilexApp\Model\Repository\PDOactionsRepository;
use SilexApp\Model\Services\OrderBySelectService;


class PublicProfileController{

    private $repository;

    public function renderPublicProfile(Application $app, $id){


        $dbs = Database::getInstance("PWGRAM","root","root");
        $this->repository = new PDOactionsRepository($dbs);

        $nom = $this->repository->getUsername($id);
        $numImg = $this->repository->getNumberImages($id);
        $numComment = $this->repository->getNumberComments($id);
        $listadoImgs = $this->repository->getImagesFromUserByCreation($id);

        //$user_id = $app['session']->get('user');

        $nomUser = $nom[0]["Username"];
        $numeroImgPublicadas = $numImg[0]["COUNT(*)"];
        $numeroComentarios = $numComment[0]["COUNT(*)"];


        $loggeado = false;
        if(empty($app['session']->get('user'))){

            $content = $app['twig']->render('publicProfileBlock8.twig', array('listadoImgs' => $listadoImgs,'user_id' => $id, 'loggeado'=>$loggeado, 'nomUser' =>$nomUser, 'numImgs'=>$numeroImgPublicadas, 'numComentarios'=>$numeroComentarios));
            $response = new Response();
            $response->setStatusCode($response::HTTP_OK);
            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);
            return $response;

        }else{
            $loggeado = true;
            $content = $app['twig']->render('publicProfileBlock8.twig', array('listadoImgs' => $listadoImgs,'user_id' => $id,'loggeado'=>$loggeado, 'nomUser' =>$nomUser, 'numImgs'=>$numeroImgPublicadas, 'numComentarios'=>$numeroComentarios));
            $response = new Response();
            $response->setStatusCode($response::HTTP_OK);
            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);
            return $response;
        }
    }

    public function renderViaSelect(Application $app, Request $request, $id){

        $selectService = new OrderBySelectService();


        $opcionOrden = $request->get('opcionOrdenar');

        switch ($opcionOrden){

            case 'Likes':
                $content = $selectService->orderByLikes($app,$id);
                $response = new Response();
                $response->setContent($content);
                return $response;
                break;

            case 'Visits':
                $content = $selectService->orderByVisits($app,$id);
                $response = new Response();
                $response->setContent($content);
                return $response;
                break;

            case 'Comments':
                $content = $selectService->orderByComments($app,$id);
                $response = new Response();
                $response->setContent($content);
                return $response;
                break;

            case 'DataCreated':
                $content = $selectService->orderByData($app,$id);
                $response = new Response();
                $response->setContent($content);
                return $response;
                break;
        }

    }

}