<?php

namespace SilexApp\Controller;
use Silex\Application;
use SilexApp\lib\Database\Database;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SilexApp\Model\Repository\PDOactionsRepository;
use DateTime;
use SilexApp\Model\Services\SignupService;
use SilexApp\Model\Services\DataBaseService;
use \Eventviva\ImageResize;



class SubirImagen{

    private $repository;

    public function renderOwner(Application $app, Request $request){
        $dbs = Database::getInstance("PWGRAM", "root", "root");
        $this->repository = new PDOactionsRepository($dbs);
        $loggeado = false;
        if(empty($app['session']->get('user'))){
            //VARGAR TEMPLATE DE CODIGO DE ERROR
            $message = 'Access Forbidden. ERROR ';
            $response = new Response();
            $response->setStatusCode($response::HTTP_FORBIDDEN);
            $code = $response->getStatusCode();

            $content = $app['twig']->render('error.twig', array('code' => $code,'message' => $message));


            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);
            return $response;
        }else{
            $img_path = $this->repository->getPathFromLoggedUser($app['session']->get('user'));
            $currentUser_path = $img_path[0]["Img_path"];
            $userID = $app['session']->get('user');
            $userName = $this->repository->getUsername($userID);
            $arrayNotificaciones = $this->repository->getNotifications($userID);
            $notificaciones = false;
            if(count($arrayNotificaciones) > 0){
                $notificaciones = true;
                //$content = $app['twig']->render('home.twig', array('loggeado' => $loggeado,'hLog' => ['hLog' => $app['app.hLog']],'titleLog' => ['titleLog' => $app['app.titleLog']],'hRegister' => ['hRegister' => $app['app.hRegister']],'titleRegister' => ['titleRegister' => $app['app.titleRegister']],'hOwner' => ['hOwner' => $app['app.vacio']],'titleOwner' => ['titleOwner' => $app['app.vacio']],'posts' => $posts, 'notificaciones' => $notificaciones));
            }

            $loggeado = true;
            $content = $app['twig']->render('owner.twig', array( 'notificaciones' => $notificaciones,'userSession' => $userName[0]["Username"],'profilePic' => $currentUser_path,'loggeado' => $loggeado,'hLog' => ['hLog' => $app['app.vacio']],'titleLog' => ['titleLog' => $app['app.vacio']],'hRegister' => ['hRegister' => $app['app.vacio']],'titleRegister' => ['titleRegister' => $app['app.vacio']],'hOwner' => ['hOwner' => $app['app.hOwner']],'titleOwner' => ['titleOwner' => $app['app.titleOwner']]));
            $response = new Response();
            $response->setStatusCode($response::HTTP_OK);
            $response->headers->set('Content-Type', 'text/html');
            $response->setContent($content);
            return $response;
        }
    }

    public function SaveImg(Application $app, Request $request){
        $dbs = Database::getInstance("PWGRAM","root","root");
        $this->repository = new PDOactionsRepository($dbs);


        $flag = false;
        $title = $request->get('title');
        //validamos el username
        if(strlen($title) > 20){
            $flag = true;
            //echo "<script type='text/javascript'>alert('Title > 20');</script>";
        }


        if (ctype_alnum(trim(str_replace(' ','',$title)))) {
            //CORRECTE
        } else {
            $flag = true;
           // echo "<script type='text/javascript'>alert('Title incorrecto');</script>";
        }
        $private = $request->get('private');
        if($private == null){
            $private = 0;

        }else{
            $private = 1;
        }


        //obtenemos el id del usuario que esta loggeado
        $user_id = $app['session']->get('user');

        $username = $this->repository->getUsername($user_id);
        //$profilepic=$_FILES['file_nm_owner']['name'];
        $profilepic1 = $request->files->get('file_nm_owner');
        $profilepic= $profilepic1->getClientOriginalName();
        //var_dump($profilepic);
        //if(is_null($profilepic)){
        //    $flag = true;
        //}
        if(is_null($profilepic1)){
            echo "<script type='text/javascript'>alert('$ _ FILES[] vacío');</script>";
            $flag = true;
        }
        $visits = 0;
        $now = new DateTime();
        $created_at = $now->format('Y-m-d H:i:s');

        $userString= $username[0]["Username"];
        if($flag == false){
            //var_dump($title);
           // var_dump($private);
           // var_dump($user_id);
            //var_dump($profilepic);
           // var_dump($created_at);
            //var_dump($userString);

            $this->repository->subirImg($title, $private, $user_id, $profilepic, $visits, $created_at, $userString);


            //file es prfilepic1
            //$file->move();

            //Primeramente guardamos la imagen subida en la carpeta
            move_uploaded_file($_FILES['file_nm_owner']['tmp_name'], '/Applications/MAMP/htdocs/RepoPracticaFinal/web/assets/img/'.$_FILES['file_nm_owner']['name']);
            //Guardamos la imagen en 2 tamaños difrentes: 400x300 i 100x100
            $image = new ImageResize('/Applications/MAMP/htdocs/RepoPracticaFinal/web/assets/img/'.$_FILES['file_nm_owner']['name']);
            $image->resize(400, 300, $allow_enlarge = True);
            $image->save('/Applications/MAMP/htdocs/RepoPracticaFinal/web/assets/img/'.'_400_'.$_FILES['file_nm_owner']['name']);

            $image100 = new ImageResize('/Applications/MAMP/htdocs/RepoPracticaFinal/web/assets/img/'.$_FILES['file_nm_owner']['name']);
            $image100->resize(100, 100, $allow_enlarge = True);
            $image100->save('/Applications/MAMP/htdocs/RepoPracticaFinal/web/assets/img/'.'_100_'.$_FILES['file_nm_owner']['name'] );

        }else{
            echo "<script type='text/javascript'>alert('Error al subir una imagen, prueba de nuevo');</script>";
            return $app->redirect('/owner');
        }

        return $app->redirect('/');
    }

}