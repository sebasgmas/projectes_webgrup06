<?php

/**
 * Created by PhpStorm.
 * User: sebastianmiguelgelabertmas
 * Date: 10/04/2017
 * Time: 19:05
 */

use AppBundle\Twig\AppExtension;
//use Symfony\Component\DependencyInjection\Definition;

$container
    ->register('app.twig_extension', prueba2::class)
    ->setPublic(false)
    ->addTag('twig.extension');