<?php
//falta la linea de cargar la home
$app->get('/','SilexApp\\Controller\\HelloController::indexAction');

$app->get('/publicPostsPoppin','SilexApp\\Controller\\PublicPostsController::publicPostsPoppin');

$app->get('/logIn','SilexApp\\Controller\\AccessLogin::renderLogIn');

$app->post('/logIn','SilexApp\\Controller\\AccessLogin::validateAction');

$app->get('/owner','SilexApp\\Controller\\SubirImagen::renderOwner');

$app->post('/owner','SilexApp\\Controller\\SubirImagen::SaveImg');

$app->get('/signup','SilexApp\\Controller\\AccessSignUp::showSignup');

$app->post('/signup','SilexApp\\Controller\\AccessSignUp::checkSignup');

$app->get('/confirmationEmail','SilexApp\\Controller\\HelloController::showConfirmation');

$app->get('/emailConfirmed/{id}','SilexApp\\Controller\\HelloController::confirmedEmail');

$app->get('/getMyPics','SilexApp\\Controller\\ManageOwnerPicsController::getMyPics');

$app->get('/remove/{id}','SilexApp\\Controller\\ManageOwnerPicsController::removeImage');

$app->get('/edit/{id}','SilexApp\\Controller\\ManageOwnerPicsController::editImage_owner');

$app->post('/edit/{id}','SilexApp\\Controller\\ManageOwnerPicsController::validateAndUpdatePic');

$app->get('/userPage','SilexApp\\Controller\\HelloController::userPage');

$app->get('/publicPost/{id}','SilexApp\\Controller\\CommentsBlock9Controller::publicPost');

$app->get('/comment/{id}','SilexApp\\Controller\\CommentsBlock9Controller::validateComment');

$app->get('/getComment','SilexApp\\Controller\\CommentsBlock9Controller::commentsList');

$app->get('/deleteComment/{id}','SilexApp\\Controller\\CommentsBlock9Controller::deleteComment');

$app->get('/editComment/{id}','SilexApp\\Controller\\CommentsBlock9Controller::editComment');

$app->get('/saveEditComment/{id}','SilexApp\\Controller\\CommentsBlock9Controller::saveEditComment');

$app->get('/like/{id}/{twig}','SilexApp\\Controller\\HelloController::likePost');

$app->get('/editProfileData','SilexApp\\Controller\\ProfileData::getDataToModify');

$app->post('/editProfileData','SilexApp\\Controller\\ProfileData::botoncito');

$app->get('/publicProfile/{id}','SilexApp\\Controller\\PublicProfileController::renderPublicProfile');

$app->post('/publicProfile/{id}','SilexApp\\Controller\\PublicProfileController::renderViaSelect');

$app->get('/showNotifications','SilexApp\\Controller\\NotificationsController::loadAndShowNotifications');

$app->get('/seenNotification/{id}','SilexApp\\Controller\\NotificationsController::checkSeenNotification');

$app->get('/darLike/{id_img}/{id_user}/{id_pagina}','SilexApp\\Controller\\LikesController::likePressed');

$app->get('/darLike/{id_img}/{id_user}/{id_pagina}','SilexApp\\Controller\\LikesController::likePressed');

$app->match('/cargarFotoAjax','SilexApp\\Controller\\AjaxController::get_persons');

$app->match('/cargarCommentsAjax','SilexApp\\Controller\\AjaxController::get_comments');

$app->match('/cargarNewFotosAjax','SilexApp\\Controller\\AjaxController::get_newPics');



//$app->get('/hello/{name}','SilexApp\\Controller\\HelloController::indexAction');
//$app->get('/add/{num1}/{num2}','SilexApp\\Controller\\HelloController::addAction');