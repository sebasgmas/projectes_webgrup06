// Wait for the DOM to be ready
$(function () {

    // Initialize form validation on the registration form.
    // It has the name attribute "registration"
    $("form[name='img']").validate({
        // Specify validation rules
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side
            title: {
                required: true,
                maxlength: 20
            }
        },
        // Specify validation error messages
        messages: {

            title: "Please enter a valid title for the picture",

        },


        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });
});