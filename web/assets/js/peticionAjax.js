$(document).ready(function() {

    $('#ajax').submit(function(event) {
        event.preventDefault();
        //var limitFotos = document.getElementById('#todasFotos').childElementCount;

       // var elements = $('.ajax_btn').attr('id');

        var elements = document.getElementById("todasFotos").childElementCount;



        var totals = elements +1;


        var data = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: '/cargarFotoAjax', //php
            data: {
                "limit": totals

            },
            dataType: 'json',
            encode: true
        })
            .done(function(response) {
                console.log(response);

               // var arrayUsar = JSON.parse(response);

                var divGeneral = document.getElementById("todasFotos");

                for(var i= 0; i< response.length; i++){

                    var img_path = response[i].Img_path;

                    var divImagen = document.createElement("div");

                    divImagen.id= "foto";
                    divImagen.className = "gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe";

                    var imagen =  document.createElement("img");

                    imagen.src ="/assets/img/" + img_path;
                    imagen.width = 400;
                    imagen.height = 400;
                    imagen.className = "img-overlay";


                    var div2 = document.createElement("div");
                    div2.className = "img-overlay";
                    div2.appendChild(imagen);

                    var div3 = document.createElement("div");
                    div3.className = "fa fa-plus project-overlay";

                    var div4 = document.createElement("div");
                    div4.className = "overlay";
                    div4.style = "text-align:center";

                    var div5 = document.createElement("div");
                    div5.className = "row";

                    var div6 = document.createElement("div");
                    div6.className = "text";

                    var span6_1 = document.createElement("span");
                    span6_1.className = "glyphicon glyphicon-heart";
                    //span6_1.aria-hidden = "true";

                    var likes = response[i].Likes;
                    var text6_2 = document.createTextNode(likes);
                    span6_1.appendChild(text6_2);
                    div6.appendChild(span6_1);

                    var div7 = document.createElement("div");
                    div7.className = "row";

                    var div8 = document.createElement("div");
                    div8.className = "text";

                    var span8_1 = document.createElement("span");
                    span8_1.className = "glyphicon glyphicon-eye-open";

                    var views = response[i].Visits;
                    var text8_1 = document.createTextNode(views);
                    span8_1.appendChild(text8_1);

                    div8.appendChild(span8_1);
                    div7.appendChild(div8);


                    div5.appendChild(div6);
                    div4.appendChild(div5);
                    div4.appendChild(div7);
                    div3.appendChild(div4);
                    div2.appendChild(imagen);
                    div2.appendChild(div3);

                    divImagen.appendChild(div2);


                    var pp = document.createElement("p");
                    divImagen.appendChild(pp);
                    divImagen.appendChild(pp);

                    var div_part2 = document.createElement("div");
                    div_part2.className = "row";

                    var part2_ul = document.createElement("ul");
                    part2_ul.className = "nav";

                    var part2_li = document.createElement("li");
                    part2_li.className = "nav-item";

                    var part2_a = document.createElement("a");
                    part2_a.className = "active";
                    var varPostId = response[i].Id;
                    part2_a.href = "/publicPost/"+varPostId;

                    var postTitle = response[i].Title;
                    var part2_a_text = document.createTextNode(postTitle);

                    part2_a.appendChild(part2_a_text);
                    part2_li.appendChild(part2_a);
                    part2_li.appendChild(part2_a);
                    part2_ul.appendChild(part2_li);
                    div_part2.appendChild(part2_ul);


                    var part3_ul = document.createElement("ul");
                    part3_ul.className = "nav";

                    var part3_li = document.createElement("li");
                    part2_li.className = "nav-item";

                    var small = document.createElement("small");

                    var date = document.createElement("span");
                    date.className = "date sub-text";

                    var fecha = response[i].Created_at;
                    var part3_date_text = document.createTextNode("Posted: " + fecha);
                    var br = document.createElement("br");
                    date.appendChild(br);
                    date.appendChild(part3_date_text);

                    var part3_a = document.createElement("a");
                    part3_a.className = "active";
                    var varPostId = response[i].User_id;
                    part3_a.href = "/publicProfile/"+varPostId;

                    var postUsername = response[i].Username;
                    var part3_a_text = document.createTextNode(postUsername);

                    part3_a.appendChild(part3_a_text);
                    small.appendChild(part3_a);
                    small.appendChild(date);
                    part3_li.appendChild(small);

                    part3_ul.appendChild(part3_li);
                    div_part2.appendChild(part3_ul);

                    divImagen.appendChild(div_part2);
                    //divImagen.appendChild(imagen);
                    divGeneral.appendChild(divImagen);



                }


            })

    });

    //Para new pics
    $('#ajaxNewPics').submit(function(event) {
        event.preventDefault();
        //var limitFotos = document.getElementById('#todasFotos').childElementCount;

        // var elements = $('.ajax_btn').attr('id');

        var elements = document.getElementById("newFotos").childElementCount;



        var totals = elements +1;


        var data = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: '/cargarNewFotosAjax', //php
            data: {
                "limit": totals

            },
            dataType: 'json',
            encode: true
        })
            .done(function(response) {
                console.log(response);

                // var arrayUsar = JSON.parse(response);

                var divGeneral = document.getElementById("newFotos");

                for(var i= 0; i< response.length; i++){

                    var img_path = response[i].Img_path;

                    var divImagen = document.createElement("div");

                    divImagen.id= "foto";
                    divImagen.className = "gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe";

                    var imagen =  document.createElement("img");

                    imagen.src ="/assets/img/" + img_path;
                    imagen.width = 400;
                    imagen.height = 400;
                    imagen.className = "img-overlay";


                    var div2 = document.createElement("div");
                    div2.className = "img-overlay";
                    div2.appendChild(imagen);

                    var div3 = document.createElement("div");
                    div3.className = "fa fa-plus project-overlay";

                    var div4 = document.createElement("div");
                    div4.className = "overlay";
                    div4.style = "text-align:center";

                    var div5 = document.createElement("div");
                    div5.className = "row";

                    var div6 = document.createElement("div");
                    div6.className = "text";

                    var span6_1 = document.createElement("span");
                    span6_1.className = "glyphicon glyphicon-heart";
                    //span6_1.aria-hidden = "true";

                    var likes = response[i].Likes;
                    var text6_2 = document.createTextNode(likes);
                    span6_1.appendChild(text6_2);
                    div6.appendChild(span6_1);

                    var div7 = document.createElement("div");
                    div7.className = "row";

                    var div8 = document.createElement("div");
                    div8.className = "text";

                    var span8_1 = document.createElement("span");
                    span8_1.className = "glyphicon glyphicon-eye-open";

                    var views = response[i].Visits;
                    var text8_1 = document.createTextNode(views);
                    span8_1.appendChild(text8_1);

                    div8.appendChild(span8_1);
                    div7.appendChild(div8);


                    div5.appendChild(div6);
                    div4.appendChild(div5);
                    div4.appendChild(div7);
                    div3.appendChild(div4);
                    div2.appendChild(imagen);
                    div2.appendChild(div3);

                    divImagen.appendChild(div2);


                    var pp = document.createElement("p");
                    divImagen.appendChild(pp);
                    divImagen.appendChild(pp);

                    var div_part2 = document.createElement("div");
                    div_part2.className = "row";

                    var part2_ul = document.createElement("ul");
                    part2_ul.className = "nav";

                    var part2_li = document.createElement("li");
                    part2_li.className = "nav-item";

                    var part2_a = document.createElement("a");
                    part2_a.className = "active";
                    var varPostId = response[i].Id;
                    part2_a.href = "/publicPost/"+varPostId;

                    var postTitle = response[i].Title;
                    var part2_a_text = document.createTextNode(postTitle);

                    part2_a.appendChild(part2_a_text);
                    part2_li.appendChild(part2_a);
                    part2_li.appendChild(part2_a);
                    part2_ul.appendChild(part2_li);
                    div_part2.appendChild(part2_ul);


                    var part3_ul = document.createElement("ul");
                    part3_ul.className = "nav";

                    var part3_li = document.createElement("li");
                    part2_li.className = "nav-item";

                    var small = document.createElement("small");

                    var date = document.createElement("span");
                    date.className = "date sub-text";

                    var fecha = response[i].Created_at;
                    var part3_date_text = document.createTextNode("Posted: " + fecha);
                    var br = document.createElement("br");
                    date.appendChild(br);
                    date.appendChild(part3_date_text);

                    var part3_a = document.createElement("a");
                    part3_a.className = "active";
                    var varPostId = response[i].User_id;
                    part3_a.href = "/publicProfile/"+varPostId;

                    var postUsername = response[i].Username;
                    var part3_a_text = document.createTextNode(postUsername);

                    part3_a.appendChild(part3_a_text);
                    small.appendChild(part3_a);
                    small.appendChild(date);
                    part3_li.appendChild(small);

                    part3_ul.appendChild(part3_li);


                    var part4_ul = document.createElement("ul");
                    part4_ul.className = "nav";

                    var part4_li = document.createElement("li");
                    part4_li.className = "nav-item";

                    var part4_div = document.createElement("div");
                    part4_div.className = "form-group";

                    var part4_label = document.createElement("label");
                    var part4_label_text = document.createTextNode("Latest comment:");

                    var part4_textarea = document.createElement("textarea");
                    part4_textarea.className = "form-control noresize";
                    part4_textarea.rows = 1;
                    part4_textarea.readOnly = true;

                    var part4_textarea_textVar = response[i].LastComment;
                    var part4_textarea_text = document.createTextNode(part4_textarea_textVar);

                    part4_textarea.appendChild(part4_textarea_text);

                    part4_label.appendChild(part4_label_text);
                    part4_div.appendChild(part4_label);
                    part4_div.appendChild(part4_textarea);
                    part4_li.appendChild(part4_div);
                    part4_ul.appendChild(part4_li);

                    div_part2.appendChild(part3_ul);
                    div_part2.appendChild(part4_ul);
                    divImagen.appendChild(div_part2);
                    //divImagen.appendChild(imagen);
                    divGeneral.appendChild(divImagen);



                }


            })

    });


    //Para new pics
    $('#ajaxNewPics').submit(function(event) {
        event.preventDefault();
        //var limitFotos = document.getElementById('#todasFotos').childElementCount;

        // var elements = $('.ajax_btn').attr('id');

        var elements = document.getElementById("newFotos").childElementCount;



        var totals = elements +1;


        var data = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: '/cargarNewFotosAjax', //php
            data: {
                "limit": totals

            },
            dataType: 'json',
            encode: true
        })
            .done(function(response) {


                // var arrayUsar = JSON.parse(response);

                var divGeneral = document.getElementById("newFotos");

                for(var i= 0; i< response.length; i++){

                    var img_path = response[i].Img_path;

                    var divImagen = document.createElement("div");

                    divImagen.id= "foto";
                    divImagen.className = "gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe";

                    var imagen =  document.createElement("img");

                    imagen.src ="/assets/img/" + img_path;
                    imagen.width = 400;
                    imagen.height = 400;
                    imagen.className = "img-overlay";


                    var div2 = document.createElement("div");
                    div2.className = "img-overlay";
                    div2.appendChild(imagen);

                    var div3 = document.createElement("div");
                    div3.className = "fa fa-plus project-overlay";

                    var div4 = document.createElement("div");
                    div4.className = "overlay";
                    div4.style = "text-align:center";

                    var div5 = document.createElement("div");
                    div5.className = "row";

                    var div6 = document.createElement("div");
                    div6.className = "text";

                    var span6_1 = document.createElement("span");
                    span6_1.className = "glyphicon glyphicon-heart";
                    //span6_1.aria-hidden = "true";

                    var likes = response[i].Likes;
                    var text6_2 = document.createTextNode(likes);
                    span6_1.appendChild(text6_2);
                    div6.appendChild(span6_1);

                    var div7 = document.createElement("div");
                    div7.className = "row";

                    var div8 = document.createElement("div");
                    div8.className = "text";

                    var span8_1 = document.createElement("span");
                    span8_1.className = "glyphicon glyphicon-eye-open";

                    var views = response[i].Visits;
                    var text8_1 = document.createTextNode(views);
                    span8_1.appendChild(text8_1);

                    div8.appendChild(span8_1);
                    div7.appendChild(div8);


                    div5.appendChild(div6);
                    div4.appendChild(div5);
                    div4.appendChild(div7);
                    div3.appendChild(div4);
                    div2.appendChild(imagen);
                    div2.appendChild(div3);

                    divImagen.appendChild(div2);


                    var pp = document.createElement("p");
                    divImagen.appendChild(pp);
                    divImagen.appendChild(pp);

                    var div_part2 = document.createElement("div");
                    div_part2.className = "row";

                    var part2_ul = document.createElement("ul");
                    part2_ul.className = "nav";

                    var part2_li = document.createElement("li");
                    part2_li.className = "nav-item";

                    var part2_a = document.createElement("a");
                    part2_a.className = "active";
                    var varPostId = response[i].Id;
                    part2_a.href = "/publicPost/"+varPostId;

                    var postTitle = response[i].Title;
                    var part2_a_text = document.createTextNode(postTitle);

                    part2_a.appendChild(part2_a_text);
                    part2_li.appendChild(part2_a);
                    part2_li.appendChild(part2_a);
                    part2_ul.appendChild(part2_li);
                    div_part2.appendChild(part2_ul);


                    var part3_ul = document.createElement("ul");
                    part3_ul.className = "nav";

                    var part3_li = document.createElement("li");
                    part2_li.className = "nav-item";

                    var small = document.createElement("small");

                    var date = document.createElement("span");
                    date.className = "date sub-text";

                    var fecha = response[i].Created_at;
                    var part3_date_text = document.createTextNode("Posted: " + fecha);
                    var br = document.createElement("br");
                    date.appendChild(br);
                    date.appendChild(part3_date_text);

                    var part3_a = document.createElement("a");
                    part3_a.className = "active";
                    var varPostId = response[i].User_id;
                    part3_a.href = "/publicProfile/"+varPostId;

                    var postUsername = response[i].Username;
                    var part3_a_text = document.createTextNode(postUsername);

                    part3_a.appendChild(part3_a_text);
                    small.appendChild(part3_a);
                    small.appendChild(date);
                    part3_li.appendChild(small);

                    part3_ul.appendChild(part3_li);


                    var part4_ul = document.createElement("ul");
                    part4_ul.className = "nav";

                    var part4_li = document.createElement("li");
                    part4_li.className = "nav-item";

                    var part4_div = document.createElement("div");
                    part4_div.className = "form-group";

                    var part4_label = document.createElement("label");
                    var part4_label_text = document.createTextNode("Latest comment:");

                    var part4_textarea = document.createElement("textarea");
                    part4_textarea.className = "form-control noresize";
                    part4_textarea.rows = 1;
                    part4_textarea.readOnly = "";

                    var part4_textarea_textVar = response[i].LastComment;
                    var part4_textarea_text = document.createTextNode(part4_textarea_textVar);

                    part4_textarea.appendChild(part4_textarea_text);

                    part4_label.appendChild(part4_label_text);
                    part4_div.appendChild(part4_label);
                    part4_div.appendChild(part4_textarea);
                    part4_li.appendChild(part4_div);
                    part4_ul.appendChild(part4_li);

                    div_part2.appendChild(part3_ul);
                    div_part2.appendChild(part4_ul);
                    divImagen.appendChild(div_part2);
                    //divImagen.appendChild(imagen);
                    divGeneral.appendChild(divImagen);



                }


            })

    });


//para comments
    $('#ajaxComments').submit(function(event) {

        event.preventDefault();
        //var limitFotos = document.getElementById('#todasFotos').childElementCount;

        var elements = document.getElementById("commentList").childElementCount;
        var id_img = $("#idRecoger").val();



        var totals = elements;


        var data = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: '/cargarCommentsAjax',
            data: {
                "limit": totals,
                "id_img": id_img

            },
            dataType: 'json',
            encode: true
        })
            .done(function(response) {



                var divGeneral = document.getElementById("commentList");

                for(var i= 0; i< response.length; i++){
                    console.log('hola');
                    var content = response[i].Content;
                    var username = response[i].Username;


                    var li = document.createElement("li");
                    var div = document.createElement("div");
                    div.className = "commentText";
                    var p = document.createElement("p");
                    var texto = document.createTextNode(content);
                    var user = document.createTextNode(username);

                    var span = document.createElement("span");
                    span.className = "date sub-text";
                    span.appendChild(user);

                    p.appendChild(texto);

                    div.appendChild(p);
                    div.appendChild(span);
                    li.appendChild(div);
                    divGeneral.appendChild(li);

                }


            })

    });




});








