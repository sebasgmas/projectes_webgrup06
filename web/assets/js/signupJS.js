function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#img-edt')
                .attr('src', e.target.result)
                .width(502)
                .height(319);
            showDimension();
        };
        reader.readAsDataURL(input.files[0]);
    }
}