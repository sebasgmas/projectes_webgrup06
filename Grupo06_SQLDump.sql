-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 18, 2017 at 06:58 PM
-- Server version: 5.6.35
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `PWGRAM`
--

-- --------------------------------------------------------

--
-- Table structure for table `Comment`
--

CREATE TABLE `Comment` (
  `Id` int(11) NOT NULL,
  `Id_img` int(11) NOT NULL,
  `Id_user` int(11) NOT NULL,
  `Content` text NOT NULL,
  `Created_at` date NOT NULL,
  `Id_userTo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Comment`
--

INSERT INTO `Comment` (`Id`, `Id_img`, `Id_user`, `Content`, `Created_at`, `Id_userTo`) VALUES
(3, 1, 8, 'laganja here', '2017-05-18', 1),
(5, 1, 1, 'im lorde ', '2017-05-18', 1),
(6, 1, 7, 'default here', '2017-05-18', 1),
(7, 1, 9, 'marc here', '2017-05-18', 1),
(8, 1, 10, 'sebas here', '2017-05-18', 1),
(9, 1, 11, 'aja here', '2017-05-18', 1),
(10, 13, 1, 'lol dat me', '2017-05-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Image`
--

CREATE TABLE `Image` (
  `Id` int(11) NOT NULL,
  `User_id` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Img_path` varchar(255) NOT NULL,
  `Visits` int(11) NOT NULL,
  `Private` tinyint(4) NOT NULL,
  `Created_at` datetime NOT NULL,
  `Likes` int(11) NOT NULL DEFAULT '0',
  `NumComments` int(11) NOT NULL DEFAULT '0',
  `Username` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Image`
--

INSERT INTO `Image` (`Id`, `User_id`, `Title`, `Img_path`, `Visits`, `Private`, `Created_at`, `Likes`, `NumComments`, `Username`) VALUES
(1, 1, 'Feelin cute', 'lorde_1.jpg', 121, 0, '2017-05-18 15:26:34', 4, 8, 'lorde'),
(2, 1, 'Lorde2', 'lordemusic-2.jpg', 9, 0, '2017-03-08 16:35:10', 0, 0, 'lorde'),
(4, 1, 'INMAMUMSCAH', 'lorde_private.jpg', 0, 0, '2017-05-18 18:16:01', 0, 0, 'lorde'),
(5, 1, 'LORDECONCHI', 'conchi_mirror.jpg', 1, 0, '2017-05-18 18:21:37', 0, 0, 'lorde'),
(6, 1, 'JESUS CHRIST', 'laganja_1.jpg', 0, 0, '2017-05-18 18:21:48', 0, 0, 'lorde'),
(7, 1, 'I LOOK CUTE', 'lorde.jpg', 0, 0, '2017-05-18 18:22:00', 0, 0, 'lorde'),
(8, 1, 'ajaaaa', 'aja.jpg', 0, 0, '2017-05-18 18:22:19', 0, 0, 'lorde'),
(9, 1, 'mah friend', 'conchi.jpg', 0, 0, '2017-05-18 18:22:42', 0, 0, 'lorde'),
(10, 1, 'greeen', 'lanja.jpg', 1, 0, '2017-05-18 18:23:01', 0, 0, 'lorde'),
(11, 1, 'selfieee', 'lordeselfie.jpg', 3, 0, '2017-05-18 18:25:01', 0, 0, 'lorde'),
(12, 1, 'gitanuki', 'lordegitanuki.jpg', 1, 0, '2017-05-18 18:25:19', 1, 0, 'lorde'),
(13, 1, 'alien', 'lordealien.jpg', 1, 0, '2017-05-18 18:25:38', 1, 1, 'lorde');

-- --------------------------------------------------------

--
-- Table structure for table `image_old`
--

CREATE TABLE `image_old` (
  `Id` int(11) NOT NULL,
  `User_id` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Img_path` varchar(255) NOT NULL,
  `Visits` int(11) NOT NULL,
  `Private` tinyint(4) NOT NULL,
  `Created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `Likes`
--

CREATE TABLE `Likes` (
  `Id` int(11) NOT NULL,
  `Id_img` int(11) NOT NULL,
  `id_User` int(11) NOT NULL COMMENT 'El usuario loggeado que da like a la foto'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Likes`
--

INSERT INTO `Likes` (`Id`, `Id_img`, `id_User`) VALUES
(7, 1, 8),
(9, 1, 1),
(11, 1, 9),
(12, 1, 10),
(13, 13, 1),
(14, 12, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Notification`
--

CREATE TABLE `Notification` (
  `Id` int(11) NOT NULL,
  `Id_img` int(11) NOT NULL,
  `Id_user` int(11) NOT NULL,
  `NotifType` int(11) NOT NULL,
  `Seen` int(11) NOT NULL DEFAULT '0',
  `UserIdTo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Notification`
--

INSERT INTO `Notification` (`Id`, `Id_img`, `Id_user`, `NotifType`, `Seen`, `UserIdTo`) VALUES
(1, 1, 1, 1, 1, 1),
(2, 1, 1, 1, 1, 1),
(3, 1, 1, 1, 1, 1),
(4, 1, 1, 1, 1, 1),
(5, 1, 8, 1, 1, 1),
(7, 1, 8, 1, 1, 1),
(8, 1, 8, 1, 1, 1),
(10, 1, 8, 0, 1, 1),
(11, 1, 1, 1, 0, 1),
(12, 1, 1, 1, 0, 1),
(13, 1, 7, 0, 0, 1),
(14, 1, 1, 0, 0, 1),
(15, 1, 7, 0, 0, 1),
(16, 1, 9, 0, 0, 1),
(17, 1, 9, 1, 0, 1),
(18, 1, 9, 1, 0, 1),
(19, 1, 10, 0, 0, 1),
(20, 1, 10, 1, 0, 1),
(21, 1, 11, 0, 0, 1),
(22, 13, 1, 0, 0, 1),
(23, 13, 1, 1, 0, 1),
(24, 12, 1, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `Id` int(11) NOT NULL,
  `Username` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Birthdate` date NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Img_path` varchar(255) NOT NULL,
  `Active` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`Id`, `Username`, `Email`, `Birthdate`, `Password`, `Img_path`, `Active`) VALUES
(1, 'lorde', 'marclex22@gmail.com', '2015-05-10', '34768743ebfa51ea8f5cf5250a2889d8', 'lorde.jpg', 1),
(2, 'conchi', 'marclex22@gmail.com', '2017-05-19', '34768743ebfa51ea8f5cf5250a2889d8', 'conchi.jpg', 1),
(7, 'default', 'marclex22@gmail.com', '2017-05-11', '34768743ebfa51ea8f5cf5250a2889d8', 'default_profile.jpg', 1),
(8, 'laganja', 'marclex22@gmail.com', '2016-11-10', '34768743ebfa51ea8f5cf5250a2889d8', 'lanja.jpg', 1),
(9, 'marc', 'marclex22@gmail.com', '2017-04-06', '34768743ebfa51ea8f5cf5250a2889d8', 'IMG_6770.jpg', 1),
(10, 'sebas', 'marclex22@gmail.com', '2017-05-17', '34768743ebfa51ea8f5cf5250a2889d8', 'kate.jpg', 1),
(11, 'aja', 'marclex22@gmail.com', '2017-05-10', '34768743ebfa51ea8f5cf5250a2889d8', 'aja.jpg', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Comment`
--
ALTER TABLE `Comment`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Image`
--
ALTER TABLE `Image`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `image_old`
--
ALTER TABLE `image_old`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Likes`
--
ALTER TABLE `Likes`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `Notification`
--
ALTER TABLE `Notification`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Comment`
--
ALTER TABLE `Comment`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `Image`
--
ALTER TABLE `Image`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `image_old`
--
ALTER TABLE `image_old`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Likes`
--
ALTER TABLE `Likes`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `Notification`
--
ALTER TABLE `Notification`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;